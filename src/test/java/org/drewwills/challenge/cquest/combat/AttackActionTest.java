package org.drewwills.challenge.cquest.combat;

import org.drewwills.challenge.cquest.CombatEngine;
import org.drewwills.challenge.cquest.Combatant;
import org.drewwills.challenge.cquest.Environment;
import org.drewwills.challenge.cquest.UserInterface;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class AttackActionTest {

    @Test
    public void testPerform() {

        final CombatEngine.ToHitOutcome toHitOutcome = CombatEngine.ToHitOutcome.CRITICAL_HIT;
        final int damageAmount = 1000;

        final Environment environment = mock(Environment.class);
        final UserInterface userInterface = mock(UserInterface.class);
        final CombatEngine combatEngine = mock(CombatEngine.class);
        when(environment.getUserInterface()).thenReturn(userInterface);
        when(environment.getCombatEngine()).thenReturn(combatEngine);

        final Combatant source = mock(Combatant.class);
        final Combatant opponent = mock(Combatant.class);

        when(combatEngine.attemptToHit(source, opponent)).thenReturn(toHitOutcome);
        when(combatEngine.calculateDamage(source, opponent, toHitOutcome)).thenReturn(damageAmount);

        final AttackAction attackAction = new AttackAction();
        attackAction.init(environment);
        attackAction.perform(source, opponent);

        verify(opponent).inflict(damageAmount);

    }

}
