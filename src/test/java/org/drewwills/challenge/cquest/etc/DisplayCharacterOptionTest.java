package org.drewwills.challenge.cquest.etc;

import org.drewwills.challenge.cquest.PlayerCharacter;
import org.drewwills.challenge.cquest.guild.AbstractPlayerCharacter;
import org.junit.Test;

import static org.junit.Assert.*;

public class DisplayCharacterOptionTest extends DisplayCharacterOption {

    private static final String CHARACTER_SHEET =
            "|-----------------------------------|\n" +
            "|Molleth                     Exp: 64|\n" +
            "|-----------------------------------|\n" +
            "|Weapon Skill:  14|Attack Power:  26|\n" +
            "|Armor:         12|Health:   28 / 28|\n" +
            "|-----------------------------------|";

    @Test
    public void testGenerateCharacterSheet() {

        final PlayerCharacter character =
                new AbstractPlayerCharacter("Molleth", 14, 26, 12, 28) {};
        character.awardExperience(64L);

        final DisplayCharacterOption displayCharacterOption = new DisplayCharacterOption();

        assertTrue("The generated character sheet does not match the expected one",
                CHARACTER_SHEET.equals(displayCharacterOption.generateCharacterSheet(character, character.getName())));

    }

}
