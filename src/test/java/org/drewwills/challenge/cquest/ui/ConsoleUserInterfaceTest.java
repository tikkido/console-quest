package org.drewwills.challenge.cquest.ui;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class ConsoleUserInterfaceTest extends ConsoleUserInterface {

    @Test
    public void testFormatTextReset() {

        final ConsoleUserInterface ui = new ConsoleUserInterface();

        Arrays.stream(TextFormat.values())
                .forEach(textFormat -> {
                    final String rawText = "foobar";
                    final String formattedText = ui.formatText(rawText, textFormat);
                    assertTrue("Formatted text does not end with ANSI reset code",
                            formattedText.endsWith(ConsoleUserInterface.RESET_CODE));
                });

    }

}
