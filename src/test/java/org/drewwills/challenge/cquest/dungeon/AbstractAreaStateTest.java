package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.AreaState;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class AbstractAreaStateTest extends AbstractAreaState {

    public AbstractAreaStateTest() {
        super("not-used", 0, 0);
    }

    @Test
    public void testCompareTo() {

        // This is the order they should be in
        final AreaState[] areas = new AreaState[] {
                new AbstractAreaStateTest("area1", 1, 2),
                new AbstractAreaStateTest("area2", 2, 2),
                new AbstractAreaStateTest("area3", 1, 1),
                new AbstractAreaStateTest("area4", 2, 1)
        };

        // Put them in a list out of order
        final List<AreaState> list = new ArrayList<>();
        list.add(areas[2]);
        list.add(areas[1]);
        list.add(areas[3]);
        list.add(areas[0]);

        // Sort them & test
        Collections.sort(list);
        for (int i=0; i < list.size(); i++) {
            assertTrue("AbstractAreaState objects are not sorted properly: " + list,
                    list.get(i).equals(areas[i]));
        }

    }

    @Override
    public Set<AreaState> getAllAreasInMaze() {
        throw new UnsupportedOperationException();
    }

    private AbstractAreaStateTest(String text, int x, int y) {
        super(text, x, y);
    }

}
