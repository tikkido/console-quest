#############################################
## Configuration Options for Console Quest ##
#############################################

## User Interface
##
## Fully-qualified name of the concrete UserInterface class.
#
org.drewwills.challenge.cquest.Environment.userInterface=org.drewwills.challenge.cquest.ui.ConsoleUserInterface

## User Options
##
## Comma-delimited list of Option classes presented by the StateMachine.  The exact options
## presented to the user at any point will be filtered by context (state).  Options will typically
## appear in the order specified here.
#
org.drewwills.challenge.cquest.Environment.stateMachineOptions=\
    org.drewwills.challenge.cquest.etc.RepeatStateOption,\
    org.drewwills.challenge.cquest.etc.QuitGameOption,\
    org.drewwills.challenge.cquest.guild.NewCharacterOption,\
    org.drewwills.challenge.cquest.io.LoadCharacterOption,\
    org.drewwills.challenge.cquest.etc.DisplayCharacterOption,\
    org.drewwills.challenge.cquest.ui.DisplayMapOption,\
    org.drewwills.challenge.cquest.guild.EnterDungeonOption,\
    org.drewwills.challenge.cquest.dungeon.GoNorthOption,\
    org.drewwills.challenge.cquest.dungeon.GoEastOption,\
    org.drewwills.challenge.cquest.dungeon.GoSouthOption,\
    org.drewwills.challenge.cquest.dungeon.GoWestOption,\
    org.drewwills.challenge.cquest.dungeon.ExitDungeonOption

## State Machine
##
## Fully-qualified name of the concrete StateMachine class.
#
org.drewwills.challenge.cquest.Environment.stateMachine=org.drewwills.challenge.cquest.io.PersistentStateMachine

## Saved Game Service
##
## Fully-qualified name of the concrete SavedGameService class.
#
org.drewwills.challenge.cquest.Environment.savedGameService=org.drewwills.challenge.cquest.io.SerializableSavedGameService

## Combat Engine
##
## Fully-qualified name of the concrete CombatEngine class, which implements a strategy for
## calculating combat-related outcomes like amount of damage inflicted.
#
org.drewwills.challenge.cquest.Environment.combatEngine=org.drewwills.challenge.cquest.combat.BasicCombatEngine

## Available Archetypes
##
## Comma-delimited list of archetypes available to the player when creating a new character.
#
org.drewwills.challenge.cquest.guild.SelectArchetypeState.availableArchetypes=\
    org.drewwills.challenge.cquest.guild.archetype.AssassinArchetype,\
    org.drewwills.challenge.cquest.guild.archetype.BerserkerArchetype,\
    org.drewwills.challenge.cquest.guild.archetype.DuelistArchetype,\
    org.drewwills.challenge.cquest.guild.archetype.KnightArchetype

## Available Dungeons
##
## Comma-delimited list of dungeons available to explore.
#
org.drewwills.challenge.cquest.guild.SelectDungeonState.availableDungeons=\
    org.drewwills.challenge.cquest.dungeon.AbyssalCavernsDungeon,\
    org.drewwills.challenge.cquest.dungeon.TempleOfCarnageDungeon

## Saved Game Directory
##
## File system location of saved game files.  Both absolute and relative paths are supported.
## Relative paths will be evaluated from the user's home directory.
#
org.drewwills.challenge.cquest.io.SerializableSavedGameService.savedGameDirectory=.cquest/saved-games

## Combat Actions
##
## Comma-delimited list of the concrete Action classes that will be offered to the user during each
## combat round.
#
org.drewwills.challenge.cquest.combat.BasicCombatEngine.combatActions=\
    org.drewwills.challenge.cquest.combat.AttackAction,\
    org.drewwills.challenge.cquest.combat.FleeAction
