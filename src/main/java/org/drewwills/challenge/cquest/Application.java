package org.drewwills.challenge.cquest;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * This class is the entry point to the application.  It provides the <code>main</code> method.
 */
public class Application extends AbstractManagedObject {

    private final Logger logger = Logger.getLogger(getClass().getName());

    /**
     * This method is the only way into the application and the only way out.  Uncaught exceptions
     * percolate up to here where they are written to <code>System.out</code>.
     */
    public static void main(String[] args) {

        // Bootstrap phase
        Application app = null;
        try {
            final Environment env = Environment.bootstrap();
            app = env.getObjectService().create(Application.class);
        } catch (Exception e) {
            e.printStackTrace(System.out);
            System.out.println("ERROR: failed to initialize the application");
            System.exit(1);
        }

        // Run phase
        app.run();

    }

    private void run() {

        final UserInterface ui = getEnv().getUserInterface();
        final InputStream splash = getClass().getResourceAsStream("splash.txt");
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(splash))) {
            logger.log(Level.INFO, "Starting Console Quest");
            ui.displayMessage(
                    ui.formatText(reader.lines().collect(Collectors.joining(System.lineSeparator())),
                            UserInterface.TextFormat.HIGHLIGHT));
            ui.displayMessage(
                    ui.formatText("Welcome to Console Quest!",
                            UserInterface.TextFormat.HIGHLIGHT));
            final State guildHall = getEnv().getGuildHall();
            getEnv().getStateMachine().start(guildHall);
            ui.displayMessage(
                    ui.formatText("Thank you for playing!\n",
                            UserInterface.TextFormat.HIGHLIGHT));
        } catch (Exception e) {
            e.printStackTrace(System.out);
            ui.displayError("Unexpected application error");
            System.exit(1);
        }

    }

}
