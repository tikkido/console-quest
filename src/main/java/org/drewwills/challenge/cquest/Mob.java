package org.drewwills.challenge.cquest;

/**
 * An enemy in a dungeon.
 */
public interface Mob extends Combatant {}
