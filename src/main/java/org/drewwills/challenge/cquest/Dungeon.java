package org.drewwills.challenge.cquest;

/**
 * Represents a dangerous area that characters can explore.
 */
public interface Dungeon extends ManagedObject {

    String getName();

    Maze getMaze();

}
