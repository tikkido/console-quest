package org.drewwills.challenge.cquest;

import java.io.Serializable;

/**
 * Every possible condition that the user's {@link PlayerCharacter} may be in is represented by an
 * instance of <code>State</code>.  Typically states will offer the user a standard,
 * context-sensitive collection of options for moving to the next state.
 *
 * <p><strong>Important Note!</strong>  Subclasses must consider that <code>State</code> objects
 * will be serialized, rehydrated, and re-bound to the {@link Environment}.  This fact has
 * implications for fields (which should be <code>Serializable</code> and/or transient as
 * appropriate), and well as methods annotated with <code>PostConstruct</code> (which must be
 * "rehydration safe").
 *
 * @see InteractiveState
 */
public interface State extends ManagedObject, Serializable {

    /**
     * A summary of the user's current status as represented by this {@link State}.
     */
    String getText();

}
