package org.drewwills.challenge.cquest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Encapsulates interactions with the user.  Provides a potential extension point.
 */
public interface UserInterface extends ManagedObject {

    /**
     * These formatting options can be applied to text that is displayed by the
     * {@link UserInterface}.
     */
    enum TextFormat {
        /**
         * Draw attention to (typically) a word or noun phrase.
         */
        HIGHLIGHT,

        /**
         * Signal to the user it's time to make a choice.
         */
        PROMPT,

        /**
         * Signal something unpleasant to the user.
         */
        WARNING,

        /**
         * Communicate a validation or (heaven forbid!) application error.
         */
        ERROR
    }

    /**
     * Different ways the user may select an option.  Options will be presented in the natural
     * order of their selection characters.
     */
    enum SelectionStrategy {

        /**
         * Select an option by number.  This is the default strategy.
         */
        BY_NUMBER {
            @Override
            public <T extends Option> Map<String,T> prepareOptions(Collection<T> options) {
                final List<T> list = new ArrayList<>(options); // index-based access
                final Map<String,T>  rslt = IntStream.range(0, options.size())
                        .boxed()
                        .collect(Collectors.toMap(
                                Object::toString,
                                list::get,
                                (v1,v2) -> {
                                        throw new RuntimeException(
                                                String.format("Duplicate key for values '%s' and '%s'",
                                                        v1.getText(), v2.getText()));
                                    },
                                LinkedHashMap::new
                        ));
                return Collections.unmodifiableMap(rslt);
            }
        },

        /**
         * Select an option by the first letter in its text.  Each option must start with a unique
         * letter, else an <code>IllegalStateException</code> will be thrown.
         */
        BY_FIRST_LETTER_LOWERCASE {
            /**
             * @throws IllegalStateException If there are duplicate keys
             */
            @Override
            public <T extends Option> Map<String,T> prepareOptions(Collection<T> options) {
                final Map<String,T> rslt = options.stream()
                        .collect(Collectors.toMap(
                                option -> option.getText().substring(0, 1).toLowerCase(),
                                option -> option,
                                (v1,v2) -> {
                                    throw new IllegalStateException(
                                            String.format("Duplicate key for values '%s' and '%s'",
                                                    v1.getText(), v2.getText()));
                                },
                                LinkedHashMap::new
                        ));
                return Collections.unmodifiableMap(rslt);
            }
        },

        /**
         * Select an option using its declared keybinding ({@link StateMachineOption}).
         */
        BY_KEY_BIND {
            /**
             * @throws ClassCastException If any option in the collection is not a
             * {@link StateMachineOption}
             */
            @Override
            public <T extends Option> Map<String,T> prepareOptions(Collection<T> options) {

                final Map<String,T>  rslt = options.stream()
                        .collect(Collectors.toMap(
                                option -> ((StateMachineOption) option).getKeyBind(),
                                option -> option,
                                (v1,v2) -> {
                                    throw new RuntimeException(
                                            String.format("Duplicate key for values '%s' and '%s'",
                                                    v1.getText(), v2.getText()));
                                },
                                LinkedHashMap::new
                        ));
                return Collections.unmodifiableMap(rslt);
            }
        };

        /**
         * Choose the appropriate user input for each option.
         */
        public abstract <T extends Option> Map<String,T> prepareOptions(Collection<T> options);

    }

    /**
     * Apply the specified formatting option to the specified <code>text</code>.
     *
     * @param text Text that will shortly be sent to the {@link UserInterface}
     * @param format One of the available text formatting options
     * @return The original <code>text</code>, but formatted in an implementation-dependant way
     */
    String formatText(String text, TextFormat format);

    /**
     * Writes the specified message to the screen after adding a blank line (for readability).
     * The message will be processed as a <em>format string</em> (see
     * <code>java.util.Formatter</code>).
     *
     * @param format A format string as described by <code>java.util.Formatter</code>
     * @param args Arguments referenced by the format specifiers in the format string (if any)
     */
    default void displayMessage(String format, Object... args) {
        displayMessage(format, true, args);
    }

    /**
     * Writes the specified message to the screen.  The message will be processed as a <em>format
     * string</em> (see <code>java.util.Formatter</code>).  Use the <code>prefixBlank</code>
     * parameter to indicate that the message should follow a new blank line.  Prefer
     * <code>prefixBlank = true</code> in most cases.
     *
     * @param format A format string as described by <code>java.util.Formatter</code>
     * @param prefixBlank Whether to separate this message from earlier messages with a blank line
     * @param args Arguments referenced by the format specifiers in the format string (if any)
     */
    void displayMessage(String format, boolean prefixBlank, Object... args);

    /**
     * Writes the specified error message to the screen with additional formatting.  The message
     * will be processed as a <em>format string</em> (see <code>java.util.Formatter</code>).
     *
     * @param format A format string as described by <code>java.util.Formatter</code>
     * @param args Arguments referenced by the format specifiers in the format string (if any)
     */
    default void displayError(String format, Object... args) {
        displayMessage(formatText("ERROR: ", TextFormat.ERROR) + format, args);
    }

    /**
     * Writes a combat log message to the screen with additional formatting.  The message will be
     * processed as a <em>format string</em> (see <code>java.util.Formatter</code>).
     *
     * @param format A format string as described by <code>java.util.Formatter</code>
     * @param args Arguments referenced by the format specifiers in the format string (if any)
     */
    default void combatLog(String format, Object... args) {
        displayMessage(" -> " + format, false, args);
    }

    // TODO: can the prompt() methods be genericized?

    /**
     * Obtains a user-selected {@link Option} from among the specified collection.  The user will be
     * able to select an option by number.
     *
     * @param options The options available to the user in this context
     * @return The option selected by the user
     */
    default <T extends Option> T promptForSelection(Collection<T> options) {
        return promptForSelection(options, SelectionStrategy.BY_NUMBER);
    }

    /**
     * Obtains a user-selected {@link Option} from among the specified collection using the
     * specified strategy.
     *
     * @param options The options available to the user in this context
     * @param selectionStrategy Strategy for presenting the options to the user
     * @return The option selected by the user
     */
    <T extends Option> T promptForSelection(Collection<T> options, SelectionStrategy selectionStrategy);

    /**
     * Obtains text input from the user based on a prompt.  The prompt will be processed as a
     * <em>format string</em> (see <code>java.util.Formatter</code>).
     *
     * @param format A format string as described by <code>java.util.Formatter</code>
     * @param args Arguments referenced by the format specifiers in the format string (if any)
     * @return Text entered by the user
     */
    String promptForInput(String format, Object... args);

}
