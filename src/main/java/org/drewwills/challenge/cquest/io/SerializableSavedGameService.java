package org.drewwills.challenge.cquest.io;

import org.drewwills.challenge.cquest.AbstractManagedObject;
import org.drewwills.challenge.cquest.Assert;
import org.drewwills.challenge.cquest.SavedGame;
import org.drewwills.challenge.cquest.SavedGameService;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Concrete implementation of {@link SavedGameService} based on Java object serialization.
 *
 * <p>There are some issues with this strategy.  For example, (1) object deserialization is an
 * abundant source of security vulnerabilities, and (2) as new versions of the game become available
 * we're likely to run into incompatibilities with previously-saved games.
 *
 * <p>Other strategies (e.g. JSON) would be more suitable long term.
 */
public class SerializableSavedGameService extends AbstractManagedObject implements SavedGameService {

    private static final String SAVED_GAME_DIRECTORY_PROPERTY =
            SerializableSavedGameService.class.getName() + ".savedGameDirectory";

    private static final String SAVED_GAME_FILE_EXTENSION = ".cquest";

    private File savedGameDirectory;

    private final Logger logger = Logger.getLogger(getClass().getName());

    @PostConstruct
    public void setup() {
        final String savedGameDirectoryProperty =
                getEnv().getProperties().get(SAVED_GAME_DIRECTORY_PROPERTY);
        Assert.notBlank(savedGameDirectoryProperty,
                "Required property is blank: " + SAVED_GAME_DIRECTORY_PROPERTY);
        final File userHomeDirectory = new File(System.getProperty("user.home"));
        savedGameDirectory = new File(userHomeDirectory, savedGameDirectoryProperty);
        if (!savedGameDirectory.exists()) {
            Assert.isTrue(savedGameDirectory.mkdirs(),
                    "Failed to create the configured savedGameDirectory: "
                            + savedGameDirectory);
        }
        logger.log(Level.INFO,"Using savedGameDirectory: {0}", savedGameDirectory);
    }

    @Override
    public void saveGame(SavedGame game) {

        // Update lastModified
        game.touch();

        final String fileName = game.getUuid().toString() + SAVED_GAME_FILE_EXTENSION;
        final File file = new File(savedGameDirectory, fileName);
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))) {
            out.writeObject(game);
        } catch (IOException ioe) {
            throw new RuntimeException("Failed to save the specified game: "
                    + game.getPlayerCharacter().getName(), ioe);
        }

    }

    @Override
    public List<SavedGame> getAvailableGames() {

        final File[] savedGameFiles = savedGameDirectory.listFiles(
                file -> file.getName().endsWith(SAVED_GAME_FILE_EXTENSION));
        logger.log(Level.INFO,"Found the following saved game files: {0}", savedGameFiles);

        final List<SavedGame> rslt = Arrays.stream(savedGameFiles)
                .map(file -> {
                    SavedGame savedGame = null; // default
                    /*
                     * Despite the performance penalty, wrap each attempt with try/catch
                     * individually so we have a chance at partial success in the event that
                     * one or more fail.
                     */
                    try (ObjectInputStream inpt = new ObjectInputStream(new FileInputStream(file))) {
                        savedGame = (SavedGame) inpt.readObject();
                    } catch (Exception e) {
                        logger.log(Level.WARNING, "Unable to load the specified savedGame file: " +
                                file.getPath(), e);
                    }
                    return savedGame;
                })
                .filter(game -> game != null)
                .collect(Collectors.toList());
        logger.log(Level.INFO,"Found the following saved games: {0}", rslt);

        return rslt;

    }

}
