package org.drewwills.challenge.cquest.io;

import org.drewwills.challenge.cquest.AbstractStateMachineOption;
import org.drewwills.challenge.cquest.GuildHallState;
import org.drewwills.challenge.cquest.PlayerCharacter;
import org.drewwills.challenge.cquest.State;

import javax.annotation.PostConstruct;

/**
 * Continue an adventure with a previously-created {@link PlayerCharacter}.
 */
public class LoadCharacterOption extends AbstractStateMachineOption {

    private SelectCharacterState selectCharacterState;

    public LoadCharacterOption() {
        super("Load an existing character", "l");
    }

    @PostConstruct
    public void setup() {
        selectCharacterState = getEnv().getObjectService().create(SelectCharacterState.class);
    }

    @Override
    public boolean appliesTo(State state) {
        return GuildHallState.class.isInstance(state);
    }

    @Override
    public State invokeUpon(State currentState) {
        return selectCharacterState;
    }

}
