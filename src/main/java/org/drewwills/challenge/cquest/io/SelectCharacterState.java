package org.drewwills.challenge.cquest.io;

import org.drewwills.challenge.cquest.AbstractState;
import org.drewwills.challenge.cquest.AreaState;
import org.drewwills.challenge.cquest.InteractiveState;
import org.drewwills.challenge.cquest.ManagedObjectService;
import org.drewwills.challenge.cquest.PayloadOption;
import org.drewwills.challenge.cquest.SavedGame;
import org.drewwills.challenge.cquest.State;
import org.drewwills.challenge.cquest.UserInterface;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Where a player selects an existing {@link SavedGame} to load.
 */
public class SelectCharacterState extends AbstractState implements InteractiveState {

    private static final String OPTION_TEXT_FORMAT = "%s (last played %s)";

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    public SelectCharacterState() {
        super("Choose a previously saved game");
    }

    @Override
    public State interact() {

        State rslt = getEnv().getGuildHall(); // default

        final List<SavedGame> savedGames = getEnv().getSavedGameService().getAvailableGames();
        final List<PayloadOption<SavedGame>> options = savedGames.stream()
                .sorted()
                .map(game -> {
                    final String optionText = String.format(
                            OPTION_TEXT_FORMAT,
                            game.getPlayerCharacter().getName(),
                            DATE_FORMAT.format(game.getLastModified()));
                    return new PayloadOption<>(optionText, game);
                })
                .collect(Collectors.toList());

        final UserInterface ui = getEnv().getUserInterface();

        // Is the list empty?
        if (options.isEmpty()) {
            ui.displayMessage(
                    ui.formatText("There are no previously saved games.",
                            UserInterface.TextFormat.HIGHLIGHT) +
                            "  Please create a new character.");
        }

        // Add an option for 'Cancel'
        final PayloadOption<SavedGame> cancelOption = new PayloadOption<>("Cancel", null);
        getEnv().getObjectService().manage(cancelOption);
        options.add(cancelOption);

        // Obtain the user's choice
        final PayloadOption<SavedGame> selectedOption = ui.promptForSelection(options);
        final SavedGame selectedGame = selectedOption.get();
        if (selectedGame != null) {
            final ManagedObjectService objectService = getEnv().getObjectService();
            // Re-bind PlayerCharacter to the Environment
            objectService.manage(selectedGame.getPlayerCharacter());
            final AreaState location = selectedGame.getLocation();
            if (location != null) {
                // Re-bind the entire Maze where the character is currently located
                location.getAllAreasInMaze().stream()
                        .forEach(area -> objectService.manage(area));
                rslt = location;
            }
            getEnv().setSavedGame(selectedGame);
        }

        return rslt;

    }

}
