package org.drewwills.challenge.cquest.io;

import org.drewwills.challenge.cquest.AbstractManagedObject;
import org.drewwills.challenge.cquest.AreaState;
import org.drewwills.challenge.cquest.Assert;
import org.drewwills.challenge.cquest.InteractiveState;
import org.drewwills.challenge.cquest.SavedGame;
import org.drewwills.challenge.cquest.State;
import org.drewwills.challenge.cquest.StateMachine;
import org.drewwills.challenge.cquest.StateMachineOption;
import org.drewwills.challenge.cquest.UserInterface;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Concrete implementation of {@link StateMachine} that sames the game after every option.
 */
public class PersistentStateMachine extends AbstractManagedObject implements StateMachine {

    private final Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public void start(State beginningState) {
        Assert.notNull(beginningState, "Argument 'beginningState' cannot be null");

        final UserInterface ui = getEnv().getUserInterface();

        State state = beginningState;
        while (state != null) {

            // Rendering
            logger.log(Level.INFO, "Rendering the following state: {0}", state);
            ui.displayMessage(state.getText());

            // Special interaction
            if (InteractiveState.class.isInstance(state)) {
                logger.log(Level.INFO, "Invoking interactive state: {0}", state);
                final State jumpToState = ((InteractiveState) state).interact();
                if (jumpToState != null) {
                    logger.log(Level.INFO, "Changed to the following state through interaction: {0}",
                            jumpToState);
                    state = jumpToState;
                    continue;
                }
            }

            final SavedGame savedGame = getEnv().getSavedGame();
            if (savedGame != null) {
                /*
                 * Save the game here.  This is the right point b/c the State is "at rest" (the user
                 * is about to make the next decision).
                 */
                final AreaState location = AreaState.class.isInstance(state)
                        ? (AreaState) state
                        : null;
                savedGame.setLocation(location);
                getEnv().getSavedGameService().saveGame(savedGame);
            }

            // Move on...
            final List<StateMachineOption> availableOptions = selectAvailableOptions(state);
            logger.log(Level.INFO, "Prompting the user to select from the following options: {0}",
                    availableOptions);
            final StateMachineOption nextOption = ui.promptForSelection(availableOptions,
                    UserInterface.SelectionStrategy.BY_KEY_BIND);
            logger.log(Level.INFO, "Invoking the following option: {0}", nextOption);
            state = nextOption.invokeUpon(state);
        }
    }


    private List<StateMachineOption> selectAvailableOptions(State state) {
        Assert.notNull(state, "Argument 'state' cannot be null");

        return getEnv().getStateMachineOptions().stream()
                .filter(option -> option.appliesTo(state))
                .collect(Collectors.toList());
    }

}
