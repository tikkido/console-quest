package org.drewwills.challenge.cquest;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * Root object for saving and reloading game state.  Only the {@link PlayerCharacter} is required
 * (else there's nothing to save).
 */
public class SavedGame implements Comparable<SavedGame>, Serializable {

    private static final long serialVersionUID = 1L;

    private final UUID uuid = UUID.randomUUID();
    private final PlayerCharacter playerCharacter;
    private AreaState location;
    private Date lastModified; // Important that this field is never null

    /**
     * The relationship between {@link SavedGame} and {@link PlayerCharacter} is 1:1.
     */
    public SavedGame(PlayerCharacter playerCharacter) {
        this.playerCharacter = playerCharacter;
        lastModified = new Date();
    }

    /**
     * Uniquely identifies the game.
     */
    public UUID getUuid() {
        return uuid;
    }

    public PlayerCharacter getPlayerCharacter() {
        return playerCharacter;
    }

    public AreaState getLocation() {
        return location;
    }

    public void setLocation(AreaState location) {
        this.location = location;
    }

    public Date getLastModified() {
        return new Date(lastModified.getTime());
    }

    public void touch() {
        this.lastModified = new Date();
    }

    // TODO: Validate a rehydrated instance?

    /**
     * The natural order of {@link SavedGame} objects is reverse chronological.  NOTE:  it is
     * possible (though very unlikely), two instances of this class could be equal in order without
     * being equal via equals().
     */
    @Override
    public int compareTo(SavedGame savedGame) {
        return savedGame.getLastModified().compareTo(lastModified);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SavedGame savedGame = (SavedGame) o;
        return Objects.equals(uuid, savedGame.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }

    @Override
    public String toString() {
        return "SavedGame{" +
                "uuid=" + uuid +
                ", playerCharacter=" + playerCharacter +
                ", location=" + location +
                ", lastModified=" + lastModified +
                '}';
    }

}
