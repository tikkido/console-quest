package org.drewwills.challenge.cquest;

import java.util.List;

/**
 * Implementations of this interface are strategies for how combat works.
 */
public interface CombatEngine extends ManagedObject {

    enum ToHitOutcome {
        MISS(false),
        HIT(true),
        CRITICAL_HIT(true);

        private final boolean hit;

        ToHitOutcome(boolean hit) {
            this.hit = hit;
        }

        public boolean isHit() {
            return hit;
        }

    }

    /**
     * Obtain a collection of options to present to the player in combat.
     */
    List<PayloadOption<Action>> getCombatOptions();

    /**
     * Select an appropriate {@link Action} for the specified {@link Mob} in combat.
     */
    Action chooseMobAction(Mob mob);

    /**
     * When a {@link Combatant} attacks, first we must determine whether the attack is a hit or a
     * miss based on (at a minimum) the relative weapon skill of the combatants.
     */
    ToHitOutcome attemptToHit(Combatant attacker, Combatant defender);

    /**
     * A {@link Combatant} may attempt to flee combat.  The odds of success are based on (at a
     * minimum) the relative speed of the combatants.
     */
    boolean attemptToFlee(Combatant hunted, Combatant hunter);

    /**
     * If the attack hits, we must calculate the amount of damage based on (at a minimum) the
     * attacker's attack power, the defender's armor, and the nature of the hit.
     */
    int calculateDamage(Combatant attacker, Combatant defender, ToHitOutcome toHitOutcome);

    /**
     * Calculates the amount of experience to award the specified {@link PlayerCharacter} for
     * defeating the specified {@link Mob}.
     */
    long calculateExperience(PlayerCharacter playerCharacter, Mob mob);

}
