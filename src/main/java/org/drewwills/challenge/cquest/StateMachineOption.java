package org.drewwills.challenge.cquest;

/**
 * Specialized sub-interface of {@link Option} for working through the {@link StateMachine}.
 */
public interface StateMachineOption extends Option {

    /**
     * Indicates whether this option is available within the specified state.
     */
    boolean appliesTo(State state);

    /**
     * The single, lowercase character that players use to select this option.
     */
    String getKeyBind();

    /**
     * Invoke this option on the <code>currentState</code> to produce a new {@State}.
     */
    State invokeUpon(State currentState);

}
