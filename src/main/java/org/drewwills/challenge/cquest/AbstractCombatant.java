package org.drewwills.challenge.cquest;

/**
 * Base class for classes that implement {@link Combatant}.
 */
public abstract class AbstractCombatant extends AbstractManagedObject implements Combatant {

    private static final long serialVersionUID = 1L;

    private final String name;
    private final int weaponSkill;
    private final int attackPower;
    private final int armor;
    private final int maxHealth;
    private final int speed;
    private int currentDamage;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getWeaponSkill() {
        return weaponSkill;
    }

    @Override
    public int getAttackPower() {
        return attackPower;
    }

    @Override
    public int getArmor() {
        return armor;
    }

    @Override
    public int getMaxHealth() {
        return maxHealth;
    }

    @Override
    public void inflict(int damage) {
        currentDamage += damage;
    }

    @Override
    public int getCurrentHealth() {
        return maxHealth - currentDamage;
    }

    @Override
    public int getSpeed() {
        return speed;
    }

    @Override
    public boolean isAlive() {
        return getCurrentHealth() > 0;
    }

    @Override
    public void recover() {
        currentDamage = 0;
    }

    @Override
    public String toString() {
        return "AbstractCombatant{" +
                "name='" + name + '\'' +
                ", weaponSkill=" + weaponSkill +
                ", attackPower=" + attackPower +
                ", armor=" + armor +
                ", maxHealth=" + maxHealth +
                ", speed=" + speed +
                ", currentHealth=" + getCurrentHealth() +
                '}';
    }

    protected AbstractCombatant(String name, int weaponSkill, int attackPower, int armor,
                                int maxHealth, int speed) {
        this.name = name;
        this.weaponSkill = weaponSkill;
        this.attackPower = attackPower;
        this.armor = armor;
        this.maxHealth = maxHealth;
        this.speed = speed;

    }

}
