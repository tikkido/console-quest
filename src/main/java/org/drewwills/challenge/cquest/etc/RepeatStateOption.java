package org.drewwills.challenge.cquest.etc;

import org.drewwills.challenge.cquest.AbstractStateMachineOption;
import org.drewwills.challenge.cquest.State;

/**
 * Re-renders the current state.
 */
public class RepeatStateOption extends AbstractStateMachineOption {

    public RepeatStateOption() {
        super("Repeat current status", "r");
    }

    /**
     * This option is always available.
     */
    @Override
    public boolean appliesTo(State state) {
        return true;
    }

    @Override
    public State invokeUpon(State currentState) {
        return currentState;
    }

}
