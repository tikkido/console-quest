package org.drewwills.challenge.cquest.etc;

import org.drewwills.challenge.cquest.AbstractStateMachineOption;
import org.drewwills.challenge.cquest.PlayerCharacter;
import org.drewwills.challenge.cquest.State;
import org.drewwills.challenge.cquest.UserInterface;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Displays the character sheet.
 */
public class DisplayCharacterOption extends AbstractStateMachineOption {

    private enum Attributes {

        /*
         * If the number of values ever becomes odd we'll have to update the formatting.
         */

        WEAPON_SKILL("Weapon Skill",
                (character) -> Integer.toString(character.getWeaponSkill())),

        ATTACK_POWER("Attack Power",
                (character) -> Integer.toString(character.getAttackPower())),

        ARMOR("Armor",
                (character) -> Integer.toString(character.getArmor())),

        HEALTH("Health",
                (character) -> character.getCurrentHealth() + " / " + character.getMaxHealth());

        private final String text;
        private final Function<PlayerCharacter,String> formatter;

        Attributes(String text, Function<PlayerCharacter,String> formatter) {
            this.text = text;
            this.formatter = formatter;
        }

        public String getText() {
            return text;
        }

        public String format(PlayerCharacter character) {
            return formatter.apply(character);
        }

    }

    public DisplayCharacterOption() {
        super("Display character information", "c");
    }

    /**
     * This option is available whenever you have a character.
     */
    @Override
    public boolean appliesTo(State state) {
        return getEnv().getSavedGame() != null;
    }

    @Override
    public State invokeUpon(State currentState) {

        final UserInterface ui = getEnv().getUserInterface();

        // Prepare & display the character sheet
        final PlayerCharacter character = getEnv().getSavedGame().getPlayerCharacter();
        final String characterName = ui.formatText(character.getName(),
                UserInterface.TextFormat.HIGHLIGHT);
        final String characterSheet = generateCharacterSheet(character, characterName);
        ui.displayMessage(characterSheet);

        return currentState;
    }

    /**
     * Prepares a multi-line character sheet containing the character's attributes.
     *
     * @param character The character whose attributes will be displayed
     * @param characterName Passed separately to support formatting
     * @return A multi-line character sheet
     */
    protected String generateCharacterSheet(PlayerCharacter character, String characterName) {

        // Gather the character's attributes
        final List<AttributeTuple> attributes = Arrays.stream(Attributes.values())
                .map(attr -> new AttributeTuple(attr.getText(), attr.format(character)))
                .collect(Collectors.toList());

        // Choose a standard length for each item based on the longest
        final int itemLength = attributes.stream()
                .mapToInt(tuple -> tuple.getLabel().length() + tuple.getValue().length())
                .max().getAsInt() + 3;

        // Compress label+value together
        final List<String> items = attributes.stream()
                .map(attr -> {
                    final int labelLength = attr.getLabel().length();
                    final int valueLength = itemLength - (labelLength + 1 /* colon */);
                    final String item = attr.getLabel() + ":" +
                            String.format("%1$" + valueLength + "s", attr.getValue());
                    return item;
                })
                .collect(Collectors.toList());

        // Build into a multi-line String
        final int experienceLength = itemLength * 2 /* columns */
                + 1 /* pipe */ - character.getName().length();
        final String experience = String.format("%1$" + experienceLength + "s",
                "Exp: " + character.getExperience());
        final String verticalSeporator = Stream.generate(() -> "-")
                .limit(itemLength * 2 + 1).collect(Collectors.joining());
        final StringBuilder builder = new StringBuilder();
        builder.append("|").append(verticalSeporator).append("|\n");
        builder.append("|").append(characterName).append(experience).append("|\n");
        builder.append("|").append(verticalSeporator).append("|\n");
        for (int i=0; i < items.size(); i++) {
            builder.append("|").append(items.get(i));
            if (i % 2 != 0) {
                builder.append("|\n");
            }
        }
        builder.append("|").append(verticalSeporator).append("|");

        return builder.toString();

    }

    private static final class AttributeTuple {

        private final String label;
        private final String value;

        public AttributeTuple(String label, String value) {
            this.label = label;
            this.value = value;
        }

        public String getLabel() {
            return label;
        }

        public String getValue() {
            return value;
        }

    }

}
