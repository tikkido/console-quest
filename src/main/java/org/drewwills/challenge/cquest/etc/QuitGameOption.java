package org.drewwills.challenge.cquest.etc;

import org.drewwills.challenge.cquest.AbstractStateMachineOption;
import org.drewwills.challenge.cquest.State;

/**
 * Exits the game.
 */
public class QuitGameOption extends AbstractStateMachineOption {

    public QuitGameOption() {
        super("Quit the game", "q");
    }

    @Override
    public boolean appliesTo(State state) {
        /*
         * The only time you can't quit is when you're interacting with an InteractiveState, and in
         * that case you won't be choosing from StateMachineOption instances.
         */
        return true;
    }

    @Override
    public State invokeUpon(State currentState) {
        return null; // Signals the state machine to finish
    }

}
