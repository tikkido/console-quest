package org.drewwills.challenge.cquest;

/**
 * Base class for {@link Option} classes that provides an implementation of <code>getText</code>.
 */
public abstract class AbstractOption extends AbstractManagedObject implements Option {

    private final String text;

    @Override
    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return String.format("%s{text='%s'}", getClass().getSimpleName(), getText());
    }

    protected AbstractOption(String text) {
        this.text = text;
    }

}
