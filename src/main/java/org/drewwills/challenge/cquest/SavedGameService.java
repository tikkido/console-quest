package org.drewwills.challenge.cquest;

import java.util.List;

/**
 * Service responsible for saving and loading {@link SavedGame} objects.
 */
public interface SavedGameService extends ManagedObject {

    /**
     * Persists the specified game, overwriting the previous copy if necessary.
     */
    void saveGame(SavedGame game);

    /**
     * Provides a complete list of previously saved games.  This method may need future enhancement
     * or changes if it causes performance issues.
     */
    List<SavedGame> getAvailableGames();

}
