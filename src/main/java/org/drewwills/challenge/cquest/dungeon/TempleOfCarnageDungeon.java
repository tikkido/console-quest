package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.Dungeon;
import org.drewwills.challenge.cquest.ManagedObjectService;
import org.drewwills.challenge.cquest.Maze;

/**
 * A low-level {@link Dungeon} for new characters.
 */
public class TempleOfCarnageDungeon extends AbstractDungeon {

    private static final String A_1_1 =
            "You stand before a massive iron gate.  The gate is open, but something tells you the " +
                    "inhabitants of this place don't need to lock it.  A hallway stretches to the " +
                    "North.";
    private static final String A_1_2 =
            "This hallway is dimly lit by candles, and you can see broken furniture and tattered " +
                    "textiles strewn across the floor.  The way goes on the the North.";
    private static final String A_1_3 =
            "The hallway widens a bit and becomes freer of debris.  There is a faint smell of " +
                    "cooking in the air.  A larger room lies ahead to the North";
    private static final String A_1_4 =
            "This is a big open space with a cooking fire in one corner.  The carcass of some " +
                    "unrecognizable creature sits roasting on a spit.  An open door leads to the " +
                    "East.";
    private static final String A_2_4 =
            "This passage curves a bit.  The ceiling is lower, though still much taller than a " +
                    "man.  You can see an open space ahead to the East.";
    private static final String A_3_4 =
            "Here the path divides:  an open door leads to the North, and the hallway continues " +
                    "to the East.";
    private static final String A_3_5 =
            "Broken bookshelves line the walls, and scraps of badly abused books line the floor.  " +
                    "Whoever lives here is not a great lover of literature.  The hall goes on to " +
                    "the North.";
    private static final String A_3_6 =
            "There are no more books;  instead, there are huge piles of pelts.  You think you can " +
                    "recognize a few of them.  Here the hall ends, but and alcove opens to the West.";
    private static final String A_2_6 =
            "This room is filthy.  A terrific number of objects have found there way here, but it " +
                    "looks like nothing has left this room since the turn of the century.  There " +
                    "are no new passages out of this room;  you must go back the way you came.";
    private static final String A_4_4 =
            "Save for a pile of straw and a few loose stones, this area is empty.  A new hallway " +
                    "opens to the South.";
    private static final String A_4_3 =
            "This path is so dark you can barely see in front of you, but you think you see a " +
                    "small light ahead to the South.";
    private static final String A_4_2 =
            "This area is flooded ankle-deep.  The hallway curves here and heads East again.";
    private static final String A_3_2 =
            "Here you encounter a long flight of stairs heading East.  There are oil paintings " +
                    "lining the walls.";
    private static final String A_5_2 =
            "At the top of the stairs you come face-to-face with a hideous statue of a man-bird " +
                    "creature.  A new hallway opens to the North";
    private static final String A_6_2 =
            "A trail of blood heads North along the floor.  Something was dragged through here " +
                    "recently.";
    private static final String A_6_3 =
            "The trail of blood ends with small, broken remains (leaning against the wall) of " +
                    "what was once probably a fairly large bear.  The way continues to the North.";
    private static final String A_6_4 =
            "Several stones from one wall have collapsed in this area and the rubble makes passage " +
                    "difficult.  You try, without much success, to make your way without noise.  " +
                    "The hallway continues to the North.";
    private static final String A_6_5 =
            "There are human remains here, though you can't tell if all the remains are from the " +
                    "same human.  It's a gory mess.  There appears to be an arch leading to a " +
                    "bigger space to the West.";
    private static final String A_5_5 =
            "This room must be the inner sanctum of the temple.  It's an ornate space with vaulted " +
                    "ceilings.  A large alter dominates one wall.  Daylight enters the room from a " +
                    "crawl space in the far corner -- a way out!";

    public TempleOfCarnageDungeon() {
        super("Temple of Carnage");
    }

    @Override
    public Maze getMaze() {
        final ManagedObjectService objectService = getEnv().getObjectService();
        final MazeBuilder mazeBuilder = MazeBuilder.enter(A_1_1, getEnv())
                .goNorth(A_1_2)
                .goNorth(A_1_3)
                .goNorth(A_1_4).encounter(objectService.create(GiantSpider.class))
                .goEast(A_2_4)
                .goEast(A_3_4)
                .goNorth(A_3_5)
                .goNorth(A_3_6)
                .goWest(A_2_6).encounter(objectService.create(GoblinSpearman.class))
                .andGoBack(3)
                .goEast(A_4_4)
                .goSouth(A_4_3)
                .goSouth(A_4_2)
                .goWest(A_3_2).encounter(objectService.create(GoblinSpearman.class))
                .andGoBack(1)
                .goEast(A_5_2)
                .goEast(A_6_2).encounter(objectService.create(SkeletalKnight.class))
                .goNorth(A_6_3)
                .goNorth(A_6_4)
                .goNorth(A_6_5)
                .goWest(A_5_5).encounter(objectService.create(StoneTroll.class))
                .exit();
        return mazeBuilder.build();
    }

}
