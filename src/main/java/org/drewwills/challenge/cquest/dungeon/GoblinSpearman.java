package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.AbstractCombatant;
import org.drewwills.challenge.cquest.Mob;

/**
 * A very basic and manageable opponent.
 */
public class GoblinSpearman extends AbstractCombatant implements Mob {

    public GoblinSpearman() {
        super("Goblin Spearman", 16, 16, 16, 16, 20);
    }

}
