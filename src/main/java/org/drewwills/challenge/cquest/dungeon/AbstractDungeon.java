package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.AbstractManagedObject;
import org.drewwills.challenge.cquest.Dungeon;

/**
 * Base class for concrete {@link Dungeon} classes.
 */
public abstract class AbstractDungeon extends AbstractManagedObject implements Dungeon {

    private final String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return String.format("%s{name='%s'}", getClass().getSimpleName(), getName());
    }

    protected AbstractDungeon(String name) {
        this.name = name;
    }

}
