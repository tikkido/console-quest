package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.AreaState;
import org.drewwills.challenge.cquest.Maze;

/**
 * Concrete {@link Maze} class for dungeons.
 */
public class DungeonMaze implements Maze {

    private final AreaState entrance;

    @Override
    public AreaState getEntrance() {
        return entrance;
    }

    /* package-private */ DungeonMaze(AreaState entrance) {
        this.entrance = entrance;
    }

}
