package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.AbstractStateMachineOption;
import org.drewwills.challenge.cquest.AreaState;
import org.drewwills.challenge.cquest.State;

/**
 * Moves the character one area to the North.
 */
public class GoNorthOption extends AbstractStateMachineOption {

    public GoNorthOption() {
        super("Go North", "n");
    }

    @Override
    public boolean appliesTo(State state) {
        return AreaState.class.isInstance(state)
                && ((AreaState) state).hasNorth();
    }

    @Override
    public State invokeUpon(State currentState) {
        return ((AreaState) currentState).getNorth();
    }

}
