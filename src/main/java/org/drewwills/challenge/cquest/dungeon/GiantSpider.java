package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.AbstractCombatant;
import org.drewwills.challenge.cquest.Mob;

/**
 * Almost as tough as a character, but not quite.
 */
public class GiantSpider extends AbstractCombatant implements Mob {

    public GiantSpider() {
        super("Giant Spider", 20, 18, 16, 22, 22);
    }

}
