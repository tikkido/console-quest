package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.AbstractStateMachineOption;
import org.drewwills.challenge.cquest.AreaState;
import org.drewwills.challenge.cquest.State;

/**
 * Moves the character one area to the East.
 */
public class GoEastOption extends AbstractStateMachineOption {

    public GoEastOption() {
        super("Go East", "e");
    }

    @Override
    public boolean appliesTo(State state) {
        return AreaState.class.isInstance(state)
                && ((AreaState) state).hasEast();
    }

    @Override
    public State invokeUpon(State currentState) {
        return ((AreaState) currentState).getEast();
    }

}
