package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.AbstractStateMachineOption;
import org.drewwills.challenge.cquest.AreaState;
import org.drewwills.challenge.cquest.State;

/**
 * Moves the character one area to the West.
 */
public class GoWestOption extends AbstractStateMachineOption {

    public GoWestOption() {
        super("Go West", "w");
    }

    @Override
    public boolean appliesTo(State state) {
        return AreaState.class.isInstance(state)
                && ((AreaState) state).hasWest();
    }

    @Override
    public State invokeUpon(State currentState) {
        return ((AreaState) currentState).getWest();
    }

}
