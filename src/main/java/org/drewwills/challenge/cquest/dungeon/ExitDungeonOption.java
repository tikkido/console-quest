package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.AbstractStateMachineOption;
import org.drewwills.challenge.cquest.AreaState;
import org.drewwills.challenge.cquest.State;

/**
 * Exits a dungeon.
 */
public class ExitDungeonOption extends AbstractStateMachineOption {

    public ExitDungeonOption() {
        super("Leave this dungeon", "x");
    }

    @Override
    public boolean appliesTo(State state) {
        return AreaState.class.isInstance(state)
                && ((AreaState) state).isExit();
    }

    @Override
    public State invokeUpon(State currentState) {
        return getEnv().getGuildHall();
    }

}
