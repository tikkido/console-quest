package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.Dungeon;
import org.drewwills.challenge.cquest.ManagedObjectService;
import org.drewwills.challenge.cquest.Maze;

/**
 * A low-level {@link Dungeon} for new characters.
 */
public class AbyssalCavernsDungeon extends AbstractDungeon {

    private static final String HIDDEN_ALCOVE =
            "You stand before a hidden alcove.  A sense of dread comes over you.  The opening " +
                    "leads to the North, and there's no telling how deep it goes.";
    private static final String DAMP_CORRIDOR =
            "You make your way into a damp corridor.  There is rubble all around you.  " +
                    "Amid the rubble you detect a few bones.  The corridor extends further to " +
                    "the North";
    private static final String CHOICE_OF_WAYS =
            "Deeper into the cavern you come to a choice of ways:  you can continue onward " +
                    "(North), or you can turn left (West)";
    private static final String LOW_PASSAGE =
            "You stoop to enter a low passage.  There is no dust on the ground beneath your " +
                    "feet -- it seems that creatures pass this way often!  The way West is open.";
    private static final String UNDERGROUND_POOL =
            "At last you come to an underground pool.  The water is clear and cold.  You cannot " +
                    "see the far side of the pool by torchlight.  You have come to a dead end.";
    private static final String TALL_DOORWAY =
            "Moving ahead you come to a tall doorway along the North wall.  Intricate carvings " +
                    "decorate the oak door.  Here and there you can also see the clumsier " +
                    "markings of axes and swords.  The door is slightly ajar.";
    private static final String LARGER_ROOM =
            "Beyond the ornate door you enter a larger room.  A foul stench saturates the air.  " +
                    "It seems that something lives here -- or died here.  There are two new " +
                    "paths out of the room and out of the reach of the stench (one hopes):  a " +
                    "slope to the East, and an exit to the North that appears to curve soon.";
    private static final String ROCKY_SLOPE =
            "Moving East you descend along a rocky slope.  The path is broken and hard to " +
                    "navigate.  You can see that the way continues to the East.";
    private static final String BEND_IN_THE_PATH =
            "Here you arrive at a bend in the path.  The walls are decorated with primitive " +
                    "drawings.  The way is clear to the South.";
    private static final String BOTTOMLESS_CHASM =
            "You reach the ledge of a chasm.  You cannot see the bottom.  You cannot continue " +
                    "this way.";
    private static final String TIGHT_CORNER =
            "The passage here is narrow and curves to the left (West).  Two humans could not " +
                    "walk abreast in this tight corner.  The way to the West is passable.";
    private static final String LONG_AISLE =
            "Emerging from confined spaces to step into a long aisle.  There are carpets and " +
                    "wall hangings of unrecognizable origin.  There is a table and two wooden " +
                    "chairs along one wall.  The aisle continues to the West.";
    private static final String WELL_LIT_ANTECHAMBER =
            "Without much warning you find yourself in a well-lit antechamber.  Lamps and " +
                    "candles cast their warm glow over the pale room.  The doorway on the North " +
                    "wall is shut, but unlocked.";
    private static final String GHASTLY_TOMB =
            "Nothing could have prepared you for the sight before you now.  You find yourself in " +
                    "an underground tomb decorated in an ancient, ghastly mode.  Statues of " +
                    "horrors and fiends line the walls.  A small shaft of light penetrates the " +
                    "gloom in one corner.  There is an exit to the surface.";

    public AbyssalCavernsDungeon() {
        super("Abyssal Caverns");
    }

    @Override
    public Maze getMaze() {
        final ManagedObjectService objectService = getEnv().getObjectService();
        final MazeBuilder mazeBuilder = MazeBuilder.enter(HIDDEN_ALCOVE, getEnv())
                .goNorth(DAMP_CORRIDOR)
                .goNorth(CHOICE_OF_WAYS)
                .goWest(LOW_PASSAGE)
                .goWest(UNDERGROUND_POOL).encounter(objectService.create(GoblinSpearman.class))
                .andGoBack(2) // back to CHOICE_OF_WAYS
                .goNorth(TALL_DOORWAY)
                .goNorth(LARGER_ROOM)
                .goEast(ROCKY_SLOPE)
                .goEast(BEND_IN_THE_PATH)
                .goSouth(BOTTOMLESS_CHASM).encounter(objectService.create(GiantSpider.class))
                .andGoBack(3) // back to LARGER_ROOM
                .goNorth(TIGHT_CORNER)
                .goWest(LONG_AISLE)
                .goWest(WELL_LIT_ANTECHAMBER)
                .goNorth(GHASTLY_TOMB).encounter(objectService.create(SkeletalKnight.class))
                .exit();
        return mazeBuilder.build();
    }

}
