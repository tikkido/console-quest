package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.AreaState;
import org.drewwills.challenge.cquest.Environment;
import org.drewwills.challenge.cquest.Maze;
import org.drewwills.challenge.cquest.Mob;

import java.util.Set;
import java.util.TreeSet;

/**
 * Facilitates building a network of doubly-connected {@link AreaState} objects for a dungeon.
 */
public class MazeBuilder extends AbstractAreaState {

    private static final long serialVersionUID = 1L;

    /**
     * The {@link MazeBuilder} instance that created this one.  Will be <code>null</code> for the
     * entrance.
     */
    private final MazeBuilder previous;

    /**
     * Every {@AreaState} in the Maze has a reference to the same collection that contains every
     * {@AreaState} in the Maze.
     */
    protected Set<AreaState> allAreas;

    /**
     * Obtain a new instance of {@link MazeBuilder} by specifying an entrance.
     *
     * @param text Text for the entrance to the {@link Maze}
     * @param env Gives this static method access to the {@link Environment}
     * @return A brand new {@link MazeBuilder} containing only an entrance
     */
    public static MazeBuilder enter(String text, Environment env) {
        final MazeBuilder rslt = new MazeBuilder(text, 1, 1, null);
        // Every entrance is automatically an exit
        rslt.exit = true;
        env.getObjectService().manage(rslt);
        return rslt;
    }

    /**
     * Adds a new area to the North.
     *
     * @param text Text for the new area
     * @return A {@link MazeBuilder} wrapping the new area
     */
    public MazeBuilder goNorth(String text) {
        final MazeBuilder rslt = new MazeBuilder(text, getX(), getY() + 1, this);
        getEnv().getObjectService().manage(rslt);
        rslt.south = this;
        north = rslt;
        return rslt;
    }

    /**
     * Adds a new area to the East.
     *
     * @param text Text for the new area
     * @return A {@link MazeBuilder} wrapping the new area
     */
    public MazeBuilder goEast(String text) {
        final MazeBuilder rslt = new MazeBuilder(text, getX() + 1, getY(), this);
        getEnv().getObjectService().manage(rslt);
        rslt.west = this;
        east = rslt;
        return rslt;
    }

    /**
     * Adds a new area to the South.
     *
     * @param text Text for the new area
     * @return A {@link MazeBuilder} wrapping the new area
     */
    public MazeBuilder goSouth(String text) {
        final MazeBuilder rslt = new MazeBuilder(text, getX(), getY() - 1, this);
        getEnv().getObjectService().manage(rslt);
        rslt.north = this;
        south = rslt;
        return rslt;
    }

    /**
     * Adds a new area to the West.
     *
     * @param text Text for the new area
     * @return A {@link MazeBuilder} wrapping the new area
     */
    public MazeBuilder goWest(String text) {
        final MazeBuilder rslt = new MazeBuilder(text, getX() - 1, getY(), this);
        getEnv().getObjectService().manage(rslt);
        rslt.east = this;
        west = rslt;
        return rslt;
    }

    /**
     * Retraces your path to a previous area <code>steps</code> backwards from where you are now.
     * Supports branching.  Throws <code>IllgalArgumentException</code> if you attempt to go further
     * back than the entrance
     *
     * @param steps The number of {@link AreaState} steps to go back
     * @return A {@link MazeBuilder} wrapping the previous area identified by <code>steps</code>
     */
    public MazeBuilder andGoBack(int steps) {
        MazeBuilder rslt = this;
        for (int i=0; i < steps; i++) {
            rslt = rslt.getPrevious();
            if (rslt == null) {
                final String message = String.format(
                        "Argument 'steps' (%d) is larger than the distance to the entrance (%d)",
                        steps, i);
                throw new IllegalArgumentException(message);
            }
        }
        return rslt;
    }

    /**
     * An area may contain at most one {@link Mob}.  The last call to this method for any area
     * "wins."
     *
     * @param mob An already-managed {@link Mob}
     * @return A {@link MazeBuilder} wrapping the same area (which now contains an encounter)
     */
    public MazeBuilder encounter(Mob mob) {
        this.mob = mob;
        return this;
    }

    /**
     * Makes this area a dungeon exit.
     *
     * @return A {@link MazeBuilder} wrapping the same area (which is now an exit)
     */
    public MazeBuilder exit() {
        this.exit = true;
        return this;
    }

    public Maze build() {

        // Rewind to the beginning...
        MazeBuilder entrance = this;
        while (entrance.getPrevious() != null) {
            entrance = entrance.getPrevious();
        }

        return new DungeonMaze(entrance);

    }

    @Override
    public Set<AreaState> getAllAreasInMaze() {
        return allAreas;
    }

    private MazeBuilder(String text, int x, int y, MazeBuilder previous) {
        super(text, x, y);
        this.previous = previous;
        allAreas = previous != null
                ? previous.getAllAreasInMaze() // Every area in the same Maze shares the same list
                : new TreeSet<>();             // We're the first area in the chain
        allAreas.add(this);
    }

    private MazeBuilder getPrevious() {
        return previous;
    }

}
