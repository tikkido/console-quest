package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.AbstractCombatant;
import org.drewwills.challenge.cquest.Mob;

/**
 * About equal to the character in combat.
 */
public class SkeletalKnight extends AbstractCombatant implements Mob {

    public SkeletalKnight() {
        super("Skeletal Knight", 18, 18, 24, 20, 18);
    }

}
