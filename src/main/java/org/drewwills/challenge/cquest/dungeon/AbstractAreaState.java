package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.AbstractState;
import org.drewwills.challenge.cquest.Action;
import org.drewwills.challenge.cquest.AreaState;
import org.drewwills.challenge.cquest.CombatEngine;
import org.drewwills.challenge.cquest.Mob;
import org.drewwills.challenge.cquest.PayloadOption;
import org.drewwills.challenge.cquest.PlayerCharacter;
import org.drewwills.challenge.cquest.State;
import org.drewwills.challenge.cquest.UserInterface;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Base class for concrete {@link AreaState} classes.
 */
public abstract class AbstractAreaState extends AbstractState implements AreaState {

    private static final long serialVersionUID = 1L;

    // Fields initialized by the builder
    protected Mob mob = null;
    protected AreaState north = null;
    protected AreaState east = null;
    protected AreaState south = null;
    protected AreaState west = null;
    protected boolean exit = false;

    // Other fields
    private final int x;
    private final int y;

    private transient Logger logger;

    @PostConstruct
    public void setup() {
        logger = Logger.getLogger(getClass().getName());
    }

    @Override
    public Mob getMob() {
        return mob;
    }

    @Override
    public boolean hasNorth() {
        return north != null;
    }

    @Override
    public AreaState getNorth() {
        return north;
    }

    @Override
    public boolean hasEast() {
        return east != null;
    }

    @Override
    public AreaState getEast() {
        return east;
    }

    @Override
    public boolean hasSouth() {
        return south != null;
    }

    @Override
    public AreaState getSouth() {
        return south;
    }

    @Override
    public boolean hasWest() {
        return west != null;
    }

    @Override
    public AreaState getWest() {
        return west;
    }

    @Override
    public boolean isExit() {
        return exit;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public int compareTo(AreaState areaState) {
        // Y has priority, descending...
        int rslt = Integer.compare(areaState.getY(), y);
        if (rslt == 0) {
            // areaState has the same Y position we do;  fall back to X acending...
            rslt = Integer.compare(x, areaState.getX());
        }
        return rslt;
    }

    @Override
    public State interact() {

        State rslt = null; // default... carry on normally

        if (mob != null) {
            rslt = mob.isAlive()
                    ? interactWithALivingMob()
                    : interactWithADeadMob();
        }

        return rslt;

    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "text=" + (getText().length() < 100
                        ? getText()
                        : getText().substring(0, 97) + "...") +
                ", mob=" + mob +
                ", exit=" + exit +
                ", x=" + x +
                ", y=" + y +
                '}';
    }

    protected AbstractAreaState(String text, int x, int y) {
        super(text);
        this.x = x;
        this.y = y;
    }

    /**
     * Combat!
     */
    private State interactWithALivingMob() {

        final UserInterface ui = getEnv().getUserInterface();
        final String mobName = ui.formatText(mob.getName(), UserInterface.TextFormat.HIGHLIGHT);

        final String introMessage =
                "Suddenly you are attacked by a %s;  prepare yourself for battle!";
        ui.displayMessage(introMessage, mobName);

        // For any outcome other than defeating the mob
        State notAWinState = null;

        // Run the combat
        final CombatEngine combatEngine = getEnv().getCombatEngine();
        final PlayerCharacter character = getEnv().getSavedGame().getPlayerCharacter();
        while (mob.isAlive() && character.isAlive() && notAWinState == null) {

            // Player first (an advantage)
            final PayloadOption<Action> selectedAction = ui.promptForSelection(
                    combatEngine.getCombatOptions(),
                    UserInterface.SelectionStrategy.BY_FIRST_LETTER_LOWERCASE
            );
            notAWinState = selectedAction.get().perform(character, mob);

            // And now the Mob, if nothing has changed
            if (mob.isAlive() && notAWinState == null) {
                final Action mobAction = combatEngine.chooseMobAction(mob);
                mobAction.perform(mob, character);
            }

        }

        // What happened?
        if (character.isAlive() && notAWinState == null) {
            // The player prevailed
            ui.displayMessage("You defeated the %s!", mobName);
            final long experience = combatEngine.calculateExperience(character, mob);
            ui.displayMessage("You gained %d experience", experience);
            character.awardExperience(experience);
        } else if (!character.isAlive()) {
            // Oh noes...
            final InputStream dead = AbstractAreaState.class.getResourceAsStream("dead.txt");
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(dead))) {
                ui.displayMessage(
                        ui.formatText(reader.lines().collect(Collectors.joining(System.lineSeparator())),
                                UserInterface.TextFormat.WARNING));
            } catch (IOException ioe) {
                logger.log(Level.WARNING, "Failed to load ASCII art 'dead.txt'", ioe);
            }
            final String deadText = ui.formatText("DEAD!", UserInterface.TextFormat.WARNING);
            ui.displayMessage("%s  You were defeated by the %s!", deadText, mobName);
            ui.promptForInput("Hit ENTER to return to the Adventurers' Guild Hall");
            notAWinState = getEnv().getGuildHall();
        }

        character.recover();

        return notAWinState != null
                ? notAWinState
                : interactWithADeadMob();

    }

    private State interactWithADeadMob() {
        final UserInterface ui = getEnv().getUserInterface();
        final String mobName = ui.formatText(mob.getName(), UserInterface.TextFormat.HIGHLIGHT);
        ui.displayMessage("A dead %s lies motionless on the floor.", mobName);
        return null;
    }

}
