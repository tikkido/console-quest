package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.AbstractCombatant;
import org.drewwills.challenge.cquest.Mob;

/**
 * A little tougher than a character
 */
public class StoneTroll extends AbstractCombatant implements Mob {

    public StoneTroll() {
        super("Stone Troll", 16, 24, 18, 28, 24);
    }

}
