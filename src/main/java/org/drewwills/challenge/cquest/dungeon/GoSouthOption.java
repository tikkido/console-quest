package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.AbstractStateMachineOption;
import org.drewwills.challenge.cquest.AreaState;
import org.drewwills.challenge.cquest.State;

/**
 * Moves the character one area to the South.
 */
public class GoSouthOption extends AbstractStateMachineOption {

    public GoSouthOption() {
        super("Go South", "s");
    }

    @Override
    public boolean appliesTo(State state) {
        return AreaState.class.isInstance(state)
                && ((AreaState) state).hasSouth();
    }

    @Override
    public State invokeUpon(State currentState) {
        return ((AreaState) currentState).getSouth();
    }

}
