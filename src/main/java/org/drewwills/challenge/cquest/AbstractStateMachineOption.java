package org.drewwills.challenge.cquest;

/**
 * Base class for {@link StateMachineOption} implementations that handles <code>getKeyBind()</code>.
 */
public abstract class AbstractStateMachineOption extends AbstractOption implements StateMachineOption {

    private final String keyBind;

    @Override
    public String getKeyBind() {
        return keyBind;
    }

    protected AbstractStateMachineOption(String text, String keyBind) {
        super(text);
        this.keyBind = keyBind;
    }

}
