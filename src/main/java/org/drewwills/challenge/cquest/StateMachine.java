package org.drewwills.challenge.cquest;

/**
 * Responsible for tracking the progress of the game.  The application will have only one instance
 * of this interface, though the concrete class is configurable.
 *
 * <p>Concrete implementations must honor the contract of {@link InteractiveState}.
 */
public interface StateMachine extends ManagedObject {

    /**
     * Begin a new game with the specified <code>beginningState</code>.  When the method returns,
     * the game exits.
     */
    void start(State beginningState);

}
