package org.drewwills.challenge.cquest;

/**
 * Root interface of the core type hierarchy within Console Quest.  Classes that implement this
 * interface have a managed lifecycle and access to the {@link Environment}.  Before an instance of
 * this interface is placed into service, its <code>init</code> method will be called, and methods
 * annotated with <code>PostConstruct</code> will also be invoked.  (Such methods must be zero-arg.)
 *
 * <p>This interface (and supporting classes) provide a very lightweight but serviceable version
 * (of some) of the benefits you get from a framework like Spring.
 */
public interface ManagedObject {

    /**
     * This method will be invoked on every {@link ManagedObject} before going into service and
     * before processing methods annotated with <code>PostConstruct</code>.
     */
    void init(Environment environment);

}
