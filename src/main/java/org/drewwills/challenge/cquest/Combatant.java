package org.drewwills.challenge.cquest;

import java.io.Serializable;

/**
 * A {@link ManagedObject} that engages in fighting.
 *
 * <p><strong>Important Note!</strong>  Subclasses must consider that <code>Combatant</code> objects
 * will be serialized, rehydrated, and re-bound to the {@link Environment}.  This fact has
 * implications for fields (which should be <code>Serializable</code> and/or transient as
 * appropriate), and well as methods annotated with <code>PostConstruct</code> (which must be
 * "rehydration safe").
 */
public interface Combatant extends ManagedObject, Serializable {

    String getName();

    /**
     * A measure of likelihood to hit and avoid being hit.
     */
    int getWeaponSkill();

    /**
     * A measure of ability to overcome armor.
     */
    int getAttackPower();

    /**
     * A measure of resistance to damage from the hits of others.
     */
    int getArmor();

    /**
     * The amount of damage that can be sustained before death.
     */
    int getMaxHealth();

    /**
     * A measure of how fast the {@link Combatant} can run when chasing or chased.
     */
    int getSpeed();

    /**
     * Reduce current health by <code>damage</code>.
     */
    void inflict(int damage);

    /**
     * The amount of health that remains (in this combat).
     */
    int getCurrentHealth();

    /**
     * Returns <code>true</code> if current health is above zero.
     */
    boolean isAlive();

    /**
     * Restores full health.  This method will be called after every combat.
     */
    void recover();

}
