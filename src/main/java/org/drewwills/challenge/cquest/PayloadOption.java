package org.drewwills.challenge.cquest;

/**
 * Specialized subclass of {@link Option} for selecting one object from among many objects of the
 * same type.
 */
public class PayloadOption<T> extends AbstractOption {

    private final T payload;

    public PayloadOption(String text, T payload) {
        super(text);
        this.payload = payload;
    }

    /**
     * Obtain this option's payload.
     */
    public T get() {
        return payload;
    }

}
