package org.drewwills.challenge.cquest;

/**
 * Base class for {@link Action} implementations.
 */
public abstract class AbstractAction extends AbstractManagedObject implements Action {

    private final String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return String.format("%s{name='%s'}", getClass().getSimpleName(), getName());
    }

    protected AbstractAction(String name) {
        this.name = name;
    }

}
