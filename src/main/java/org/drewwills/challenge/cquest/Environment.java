package org.drewwills.challenge.cquest;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * This Singleton class provides access to the essential subsystems within Console Quest.  Every
 * {@link ManagedObject} instance in the application has access to it.
 */
public class Environment {

    /**
     * Singleton instance of this class, which will be created by the (static)
     * <code>bootstrap</code> method.
     */
    private static Environment instance;

    /**
     * Location of the logger.properties file on the classpath.
     */
    private static final String LOGGER_PROPERTIES_FILENAME = "/logger.properties";

    /**
     * Location of the application.properties file on the classpath.
     */
    private static final String APPLICATION_PROPERTIES_FILENAME = "/application.properties";

    /**
     * Name of the application property that defines the concrete {@link UserInterface} class.
     */
    private static final String USER_INTERFACE_CLASSNAME_PROPERTY =
            Environment.class.getName() + ".userInterface";

    /**
     * Name of the application property that defines the complete collection of options available to
     * users throughout the game.  The options available at any one time are based on context
     * (state).
     */
    private static final String STATE_MACHINE_OPTIONS_PROPERTY =
            Environment.class.getName() + ".stateMachineOptions";

    /**
     * Name of the application property that defines the concrete {@link UserInterface} class.
     */
    private static final String STATE_MACHINE_CLASSNAME_PROPERTY =
            Environment.class.getName() + ".stateMachine";

    private static final String SAVED_GAME_SERVICE_PROPERTY =
            Environment.class.getName() + ".savedGameService";

    private static final String COMBAT_ENGINE_PROPERTY =
            Environment.class.getName() + ".combatEngine";

    private final Map<String,String> properties;
    private final ManagedObjectService objectService;
    private final UserInterface userInterface;
    private final List<StateMachineOption> stateMachineOptions;
    private final State guildHall;
    private final StateMachine stateMachine;
    private final SavedGameService savedGameService;
    private final CombatEngine combatEngine;

    private SavedGame savedGame;

    private final Logger logger = Logger.getLogger(getClass().getName());

    /**
     * Bootsraps the application environment.  This method should be called once (by the Application
     * class) when the game launches.  Calling this method more than once results in an
     * <code>IllegalStateException</code> since bootstrapping the application more than once sounds
     * like a bad idea.
     *
     * <p>This method is <code>synchronized</code> in case this application ever be comes
     * multi-threaded and because the performance penulty will only be paid once.
     *
     * @return The singleton instance of {@link Environment}
     * @throws IllegalStateException If called more than once
     */
    /* package-private */ static synchronized Environment bootstrap() {

        // This method may only be invoked once...
        if (instance != null) {
            throw new IllegalStateException("The Environment has already been bootstrapped");
        }

        // Set up logging with the JDK's LogManager
        try(InputStream inpt = Environment.class.getResourceAsStream(LOGGER_PROPERTIES_FILENAME)) {
            LogManager.getLogManager().readConfiguration(inpt);
        } catch (Exception e) {
            throw new RuntimeException("Failed to initialize the LogManager", e);
        }

        // Application properties
        final Properties properties = new Properties();
        try (final InputStream inpt = Environment.class.getResourceAsStream(APPLICATION_PROPERTIES_FILENAME)) {
            properties.load(inpt);
        } catch (Exception e) {
            throw new RuntimeException("Unable to load application properties", e);
        }
        final Map<String, String> propertiesMap = properties.entrySet().stream()
                .collect(
                        Collectors.toMap(
                                e -> (String) e.getKey(),
                                e -> (String) e.getValue()
                        ));

        instance = new Environment(propertiesMap);
        return instance;

    }

    /**
     * Provides instances of {@link ManagedObject} with access to application settings.
     */
    public Map<String, String> getProperties() {
        return properties;
    }

    public ManagedObjectService getObjectService() {
        return objectService;
    }

    public UserInterface getUserInterface() {
        return userInterface;
    }

    // TODO:  Move to StateMachine?
    public List<StateMachineOption> getStateMachineOptions() {
        return stateMachineOptions;
    }

    public State getGuildHall() {
        return guildHall;
    }

    public StateMachine getStateMachine() {
        return stateMachine;
    }

    public SavedGameService getSavedGameService() {
        return savedGameService;
    }

    public CombatEngine getCombatEngine() {
        return combatEngine;
    }

    public SavedGame getSavedGame() {
        return savedGame;
    }

    public void setSavedGame(SavedGame savedGame) {
        this.savedGame = savedGame;
    }

    /**
     * Private constructor so that instances of this class may not be created outside of the class
     * itself.
     */
    private Environment(Map<String,String> propertiesMap) {

        properties = Collections.unmodifiableMap(propertiesMap);

        objectService = new ManagedObjectService(this);

        final String userInterfaceClass = properties.get(USER_INTERFACE_CLASSNAME_PROPERTY);
        Assert.notBlank(userInterfaceClass,
                "Required property is blank: " + USER_INTERFACE_CLASSNAME_PROPERTY);
        userInterface = objectService.create(userInterfaceClass, UserInterface.class);
        logger.log(Level.INFO,"Using userInterface: {0}", userInterface);

        final String stateMachineOptionsProperty = properties.get(STATE_MACHINE_OPTIONS_PROPERTY);
        Assert.notBlank(stateMachineOptionsProperty,
                "Required property is blank: " + STATE_MACHINE_OPTIONS_PROPERTY);
        final List<StateMachineOption> options =
                Arrays.stream(stateMachineOptionsProperty.split(","))
                        .map(optionClass -> objectService.create(optionClass,
                                StateMachineOption.class))
                        .collect(Collectors.toList());
        stateMachineOptions = Collections.unmodifiableList(options);
        logger.log(Level.INFO,"Found the following state machine options: {0}", stateMachineOptions);

        guildHall = objectService.create(GuildHallState.class);

        final String stateMachineClass = properties.get(STATE_MACHINE_CLASSNAME_PROPERTY);
        Assert.notBlank(stateMachineClass,
                "Required property is blank: " + STATE_MACHINE_CLASSNAME_PROPERTY);
        stateMachine = objectService.create(stateMachineClass, StateMachine.class);
        logger.log(Level.INFO,"Using stateMachine: {0}", stateMachine);

        final String savedGameServiceProperty = properties.get(SAVED_GAME_SERVICE_PROPERTY);
        Assert.notBlank(savedGameServiceProperty,
                "Required property is blank: " + SAVED_GAME_SERVICE_PROPERTY);
        savedGameService = objectService.create(savedGameServiceProperty, SavedGameService.class);
        logger.log(Level.INFO,"Using savedGameService: {0}", savedGameService);

        final String combatEngineProperty = properties.get(COMBAT_ENGINE_PROPERTY);
        Assert.notBlank(combatEngineProperty,
                "Required property is blank: " + COMBAT_ENGINE_PROPERTY);
        combatEngine = objectService.create(combatEngineProperty, CombatEngine.class);
        logger.log(Level.INFO,"Using combatEngine: {0}", combatEngine);

    }

}
