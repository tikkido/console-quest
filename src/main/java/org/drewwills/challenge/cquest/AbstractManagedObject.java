package org.drewwills.challenge.cquest;

/**
 * Provides a straightforward implementation of the <code>init</code> method that holds onto the
 * {@link Environment}.  Subclasses of may specify their own initialization, if required, by
 * annotating a public, void-returning, zero-argument method with <code>@PostConstruct</code>.
 */
public abstract class AbstractManagedObject implements ManagedObject {

    /*
     * A small number of ManagedObject implementations can be persisted via Java Object
     * Serialization.  The Environment is a Singleton, and must not be included in that process.
     * Rehydrated ManagedObject instances must be reconnected with the Environment using
     * ManagedObjectService.manage().
     */
    private transient Environment env;

    /**
     * Holds onto the {@link Environment} for later use.
     */
    @Override
    public void init(Environment env) {
        this.env = env;
    }

    /**
     * Subclasses my access the {@link Environment} with this method.
     */
    protected Environment getEnv() {
        return env;
    }

}
