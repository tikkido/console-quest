package org.drewwills.challenge.cquest.guild;

import org.drewwills.challenge.cquest.AbstractStateMachineOption;
import org.drewwills.challenge.cquest.GuildHallState;
import org.drewwills.challenge.cquest.State;

import javax.annotation.PostConstruct;

/**
 * Presents a list of dungeons and allows the player to enter one.
 */
public class EnterDungeonOption extends AbstractStateMachineOption {


    private SelectDungeonState selectDungeonState;

    public EnterDungeonOption() {
        super("Enter a dungeon!", "d");
    }

    @PostConstruct
    public void setup() {
        selectDungeonState = getEnv().getObjectService().create(SelectDungeonState.class);
    }

    @Override
    public boolean appliesTo(State state) {
        return GuildHallState.class.isInstance(state)
                && getEnv().getSavedGame() != null;
    }

    @Override
    public State invokeUpon(State currentState) {
        return selectDungeonState;
    }

}
