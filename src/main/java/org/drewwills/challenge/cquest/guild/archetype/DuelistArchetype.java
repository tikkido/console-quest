package org.drewwills.challenge.cquest.guild.archetype;

import org.drewwills.challenge.cquest.PlayerCharacter;
import org.drewwills.challenge.cquest.guild.Archetype;

/**
 * {@link Archetype} implementation for {@link Duelist} characters.
 */
public class DuelistArchetype extends AbstractArchetype {

    public DuelistArchetype() {
        super("Duelist",
                "A veteran fighter with high weapon skill, low damage, and average armor and health");
    }

    @Override
    public PlayerCharacter create(String name) {
        return new Duelist(name);
    }

}
