package org.drewwills.challenge.cquest.guild.archetype;

import org.drewwills.challenge.cquest.guild.AbstractPlayerCharacter;

/**
 * Veteran fighter with high weapon skill, low damage, and average armor and health.
 */
public class Duelist extends AbstractPlayerCharacter {

    /* package-private */ Duelist(String name) {
        super(name, 26, 14, 20, 20);
    }

}
