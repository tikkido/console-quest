package org.drewwills.challenge.cquest.guild;

import org.drewwills.challenge.cquest.AbstractState;
import org.drewwills.challenge.cquest.Assert;
import org.drewwills.challenge.cquest.ManagedObject;
import org.drewwills.challenge.cquest.Maze;
import org.drewwills.challenge.cquest.PayloadOption;
import org.drewwills.challenge.cquest.PlayerCharacter;
import org.drewwills.challenge.cquest.Dungeon;
import org.drewwills.challenge.cquest.InteractiveState;
import org.drewwills.challenge.cquest.State;
import org.drewwills.challenge.cquest.UserInterface;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Where a {@link PlayerCharacter} selects a {@link Dungeon} to enter.
 */
public class SelectDungeonState extends AbstractState implements InteractiveState {

    /**
     * Name of the application property that defines the complete collection of availableDungeons available
     * for the player to explore.
     */
    private static final String AVAILABLE_DUNGEONS_PROPERTY = SelectDungeonState.class.getName() + ".availableDungeons";

    private static final long serialVersionUID = 1L;

    /**
     * Transient because {@link SelectArchetypeState} is a <code>Serializable</code> subtype of
     * {@link ManagedObject} and this field is initialized in a method annotated with
     * <code>PostConstruct</code>.
     */
    private transient List<PayloadOption<Dungeon>> availableDungeons;

    /**
     * Transient because {@link SelectArchetypeState} is a <code>Serializable</code> subtype of
     * {@link ManagedObject} and this field is initialized in a method annotated with
     * <code>PostConstruct</code>.
     */
    private transient Logger logger;

    public SelectDungeonState() {
        super("Choose a dungeon");
    }

    @PostConstruct
    public void setup() {

        // logger
        logger = Logger.getLogger(getClass().getName());

        // Available dungeons
        final String availableDungeonsProperty = getEnv().getProperties().get(AVAILABLE_DUNGEONS_PROPERTY);
        Assert.notBlank(availableDungeonsProperty,
                "Required property is blank: " + AVAILABLE_DUNGEONS_PROPERTY);
        final List<Dungeon> dungeons = Arrays.stream(availableDungeonsProperty.split(","))
                .map(dungeonClass -> getEnv().getObjectService().create(dungeonClass, Dungeon.class))
                .collect(Collectors.toList());
        logger.log(Level.INFO,"Found the following available dungeons: {0}", dungeons);
        final List<PayloadOption<Dungeon>> optionsList = dungeons.stream()
                .map(dungeon -> new PayloadOption<>(dungeon.getName(), dungeon))
                .map(option -> getEnv().getObjectService().manage(option))
                .collect(Collectors.toList());
        final PayloadOption<Dungeon> cancelOption = new PayloadOption<>("Cancel", null);
        getEnv().getObjectService().manage(cancelOption);
        optionsList.add(cancelOption);
        availableDungeons = Collections.unmodifiableList(optionsList);

    }

    @Override
    public State interact() {
        final UserInterface ui = getEnv().getUserInterface();
        final PayloadOption<Dungeon> selectedOption = ui.promptForSelection(availableDungeons);
        final Dungeon dungeon = selectedOption.get();
        if (dungeon != null) {
            final Maze maze = dungeon.getMaze();
            return maze.getEntrance();
        }
        // User canceled
        return getEnv().getGuildHall();
    }

}
