package org.drewwills.challenge.cquest.guild;

import org.drewwills.challenge.cquest.AbstractState;
import org.drewwills.challenge.cquest.InteractiveState;
import org.drewwills.challenge.cquest.PlayerCharacter;
import org.drewwills.challenge.cquest.SavedGame;
import org.drewwills.challenge.cquest.State;
import org.drewwills.challenge.cquest.UserInterface;

import java.util.regex.Pattern;

public class EnterNameState extends AbstractState implements InteractiveState {

    private static final String FAILED_VALIDATION_FEEDBACK =
            "Character names may contain between 4 and 20 word characters and spaces, and must " +
            "begin and end with a word character.  Word characters are a-z, A-Z, 0-9, and _.";

    private final Pattern nameValidationPattern = Pattern.compile("\\w[\\w ]{2,18}\\w");

    private Archetype selectedArchetype;

    public EnterNameState() {
        super("Choose a character name");
    }

    public void setSelectedArchetype(Archetype selectedArchetype) {
        this.selectedArchetype = selectedArchetype;
    }

    @Override
    public State interact() {
        final UserInterface ui = getEnv().getUserInterface();

        String name = null;
        while (name == null) {
            final String input = ui.promptForInput("Name");
            if (!nameValidationPattern.matcher(input).matches()) {
                ui.displayError(FAILED_VALIDATION_FEEDBACK);
            } else {
                name = input;
            }
        }

        final PlayerCharacter character = selectedArchetype.create(name);
        getEnv().setSavedGame(new SavedGame(character));
        return getEnv().getGuildHall();
    }

}
