package org.drewwills.challenge.cquest.guild.archetype;

import org.drewwills.challenge.cquest.PlayerCharacter;
import org.drewwills.challenge.cquest.guild.Archetype;

/**
 * {@link Archetype} implementation for {@link Berserker} characters.
 */
public class BerserkerArchetype extends AbstractArchetype {

    public BerserkerArchetype() {
        super("Berserker",
                "A warrior with high damage and health, low weapon skill and armor");
    }

    @Override
    public PlayerCharacter create(String name) {
        return new Berserker(name);
    }

}
