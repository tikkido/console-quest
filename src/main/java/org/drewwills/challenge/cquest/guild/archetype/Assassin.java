package org.drewwills.challenge.cquest.guild.archetype;

import org.drewwills.challenge.cquest.guild.AbstractPlayerCharacter;

/**
 * Rogue with high weapon skill and damage, low armor and health.
 */
public class Assassin extends AbstractPlayerCharacter {

    /* package-private */ Assassin(String name) {
        super(name, 24, 26, 12, 18);
    }

}
