package org.drewwills.challenge.cquest.guild.archetype;

import org.drewwills.challenge.cquest.PlayerCharacter;
import org.drewwills.challenge.cquest.guild.Archetype;

/**
 * {@link Archetype} implementation for {@link Knight} characters.
 */
public class KnightArchetype extends AbstractArchetype {

    public KnightArchetype() {
        super("Knight",
                "A thick-skinned warrior with high armor and health, low weapon skill and damage");
    }

    @Override
    public PlayerCharacter create(String name) {
        return new Knight(name);
    }

}
