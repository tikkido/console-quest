package org.drewwills.challenge.cquest.guild.archetype;

import org.drewwills.challenge.cquest.AbstractManagedObject;
import org.drewwills.challenge.cquest.guild.Archetype;

/**
 * Base class for implementations of {@link Archetype}.
 */
public abstract class AbstractArchetype extends AbstractManagedObject implements Archetype {

    private final String name;
    private final String description;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    /* package-private */ AbstractArchetype(String name, String description) {
        this.name = name;
        this.description = description;
    }

}
