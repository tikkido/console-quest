package org.drewwills.challenge.cquest.guild;

import org.drewwills.challenge.cquest.AbstractStateMachineOption;
import org.drewwills.challenge.cquest.GuildHallState;
import org.drewwills.challenge.cquest.PlayerCharacter;
import org.drewwills.challenge.cquest.State;

import javax.annotation.PostConstruct;

/**
 * Choose to begin a new adventure with a new {@link PlayerCharacter}.
 */
public class NewCharacterOption extends AbstractStateMachineOption {

    private SelectArchetypeState selectArchetypeState;

    public NewCharacterOption() {
        super("Create a new character", "h");
    }

    @PostConstruct
    public void setup() {
        selectArchetypeState = getEnv().getObjectService().create(SelectArchetypeState.class);
    }

    @Override
    public boolean appliesTo(State state) {
        return GuildHallState.class.isInstance(state);
    }

    @Override
    public State invokeUpon(State currentState) {
        return selectArchetypeState;
    }

}
