package org.drewwills.challenge.cquest.guild;

import org.drewwills.challenge.cquest.AbstractState;
import org.drewwills.challenge.cquest.Assert;
import org.drewwills.challenge.cquest.InteractiveState;
import org.drewwills.challenge.cquest.ManagedObject;
import org.drewwills.challenge.cquest.PayloadOption;
import org.drewwills.challenge.cquest.State;
import org.drewwills.challenge.cquest.UserInterface;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Where a player selects an {@link Archetype} for a new {@link Character}.
 */
public class SelectArchetypeState extends AbstractState implements InteractiveState {

    private static final String AVAILABLE_ARCHETYPES_PROPERTY =
            SelectArchetypeState.class.getName() + ".availableArchetypes";

    private static final long serialVersionUID = 1L;

    /**
     * Transient because {@link SelectArchetypeState} is a <code>Serializable</code> subtype of
     * {@link ManagedObject} and this field is initialized in a method annotated with
     * <code>PostConstruct</code>.
     */
    private transient Set<PayloadOption<Archetype>> archetypeOptions;

    private EnterNameState enterNameState;

    /**
     * Transient because {@link SelectArchetypeState} is a <code>Serializable</code> subtype of
     * {@link ManagedObject} and this field is initialized in a method annotated with
     * <code>PostConstruct</code>.
     */
    private transient Logger logger;

    public SelectArchetypeState() {
        super("Choose an archetype");
    }

    @PostConstruct
    public void setup() {

        // Logger
        logger = Logger.getLogger(getClass().getName());

        // Available archetypes
        final String availableArchetypesProperty = getEnv().getProperties().get(AVAILABLE_ARCHETYPES_PROPERTY);
        Assert.notBlank(availableArchetypesProperty,
                "Required property is blank: " + AVAILABLE_ARCHETYPES_PROPERTY);
        final Set<Archetype> archetypes = Arrays.stream(availableArchetypesProperty.split(","))
                .map(archetypeClass -> getEnv().getObjectService().create(archetypeClass, Archetype.class))
                .collect(Collectors.toSet());
        logger.log(Level.INFO,"Found the following character archetypes: {0}", archetypes);
        archetypeOptions = Collections.unmodifiableSet(archetypes.stream()
                .map(archetype -> new PayloadOption<>(archetype.getName(), archetype))
                .map(archetype -> getEnv().getObjectService().manage(archetype))
                .collect(Collectors.toSet()));

        // Enter name
        enterNameState = getEnv().getObjectService().create(EnterNameState.class);

    }

    @Override
    public State interact() {
        final UserInterface ui = getEnv().getUserInterface();
        final PayloadOption<Archetype> selectedOption = ui.promptForSelection(archetypeOptions,
                        UserInterface.SelectionStrategy.BY_FIRST_LETTER_LOWERCASE);
        final Archetype archetype = selectedOption.get();
        enterNameState.setSelectedArchetype(archetype);
        return enterNameState;
    }

}
