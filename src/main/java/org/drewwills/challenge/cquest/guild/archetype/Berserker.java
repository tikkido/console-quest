package org.drewwills.challenge.cquest.guild.archetype;

import org.drewwills.challenge.cquest.guild.AbstractPlayerCharacter;

/**
 * Warrior with high damage and health, low weapon skill and armor.
 */
public class Berserker extends AbstractPlayerCharacter {

    /* package-private */ Berserker(String name) {
        super(name, 14, 26, 12, 28);
    }

}
