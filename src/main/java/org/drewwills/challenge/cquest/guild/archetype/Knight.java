package org.drewwills.challenge.cquest.guild.archetype;

import org.drewwills.challenge.cquest.guild.AbstractPlayerCharacter;

/**
 * Warrior with high armor and health, low weapon skill and damage.
 */
public class Knight extends AbstractPlayerCharacter {

    /* package-private */ Knight(String name) {
        super(name, 16, 16, 26, 22);
    }

}
