package org.drewwills.challenge.cquest.guild;

import org.drewwills.challenge.cquest.ManagedObject;
import org.drewwills.challenge.cquest.PlayerCharacter;

/**
 * A factory for creating {@link Character} objects of a specific concrete type.
 */
public interface Archetype extends ManagedObject {

    String getName();

    String getDescription();

    PlayerCharacter create(String name);

}
