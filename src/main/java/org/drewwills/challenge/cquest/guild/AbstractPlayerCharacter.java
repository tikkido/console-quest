package org.drewwills.challenge.cquest.guild;

import org.drewwills.challenge.cquest.AbstractCombatant;
import org.drewwills.challenge.cquest.PlayerCharacter;

public class AbstractPlayerCharacter extends AbstractCombatant implements PlayerCharacter {

    private static final long serialVersionUID = 1L;

    private long experience = 0L;

    @Override
    public void awardExperience(long exp) {
        experience += exp;
    }

    @Override
    public long getExperience() {
        return experience;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "name='" + getName() + '\'' +
                ", weaponSkill=" + getWeaponSkill() +
                ", attackPower=" + getAttackPower() +
                ", armor=" + getArmor() +
                ", maxHealth=" + getMaxHealth() +
                ", currentHealth=" + getCurrentHealth() +
                ", experience=" + getExperience() +
                '}';
    }

    protected AbstractPlayerCharacter(String name, int weaponSkill, int attackPower, int armor,
            int maxHealth) {
        super(name, weaponSkill, attackPower, armor, maxHealth, 20);
    }

}
