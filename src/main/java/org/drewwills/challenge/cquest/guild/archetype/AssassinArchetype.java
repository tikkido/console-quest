package org.drewwills.challenge.cquest.guild.archetype;

import org.drewwills.challenge.cquest.PlayerCharacter;
import org.drewwills.challenge.cquest.guild.Archetype;

/**
 * {@link Archetype} implementation for {@link Assassin} characters.
 */
public class AssassinArchetype extends AbstractArchetype {

    public AssassinArchetype() {
        super("Assassin",
                "A rogue with high weapon skill and damage, low armor and health");
    }

    @Override
    public PlayerCharacter create(String name) {
        return new Assassin(name);
    }

}
