package org.drewwills.challenge.cquest;

/**
 * A player character in Console Quest.
 */
public interface PlayerCharacter extends Combatant {

    /**
     * Player characters gain experience by overcoming mobs as they explore.
     */
    void awardExperience(long exp);

    /**
     * Returns the amount of experience the character has gained so far.
     */
    long getExperience();

}
