package org.drewwills.challenge.cquest;

import java.util.Collection;

/**
 * Provides some utility methods for validating arguments.  Each method throws
 * <code>IllegalArgumentException</code> if its expectations are not met.
 */
public class Assert {

    public static void isTrue(boolean b, String message) {
        if (!b) {
            throw new IllegalStateException(message);
        }
    }

    public static void notNull(Object o, String message) {
        if (o == null) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void notBlank(String s, String message) {
        if (Check.isBlank(s)) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void notEmpty(Collection c, String message) {
        if (Check.isEmpty(c)) {
            throw new IllegalArgumentException(message);
        }
    }

}
