package org.drewwills.challenge.cquest;

import java.util.Collection;

/**
 * Provides some utility methods for testing values.  Each method returns
 * <code>false</code> if its expectations are not met.
 */
public class Check {

    public static boolean isBlank(String s) {
        return s == null || s.trim().length() == 0;
    }

    public static boolean isEmpty(Collection c) {
        return c == null || c.size() == 0;
    }

}
