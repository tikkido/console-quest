package org.drewwills.challenge.cquest.ui;

import org.drewwills.challenge.cquest.AbstractStateMachineOption;
import org.drewwills.challenge.cquest.AreaState;
import org.drewwills.challenge.cquest.State;
import org.drewwills.challenge.cquest.UserInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Displays the character sheet.
 */
public class DisplayMapOption extends AbstractStateMachineOption {

    private static final String BLOCKED_CHARACTER = "@";
    private static final String UNBLOCKED_CHUNK = "   ";
    private static final String LOCATION_MARKER_CHUNK = " * ";

    private final Logger logger = Logger.getLogger(getClass().getName());

    public DisplayMapOption() {
        super("Display a dungeon map", "m");
    }

    /**
     * This option is available when you are in a dungeon.
     */
    @Override
    public boolean appliesTo(State state) {
        return AreaState.class.isInstance(state);
    }

    @Override
    public State invokeUpon(State currentState) {

        logger.log(Level.FINE, "Preparing a dungeon map for player location: " + currentState);

        final UserInterface ui = getEnv().getUserInterface();

        // Prepare the dungeon map
        String dungeonMap = generateDungeonMap((AreaState) currentState);

        // Last step:  format the location marker
        dungeonMap = dungeonMap.replace(LOCATION_MARKER_CHUNK,
                ui.formatText(LOCATION_MARKER_CHUNK, UserInterface.TextFormat.HIGHLIGHT));

        ui.displayMessage(dungeonMap);

        return currentState;
    }

    /**
     * Prepares an ASCII-based map of the dungeon.  NOTE: This implementation assumes the entrance to
     * the dungeon -- which is always (1,1) -- is the only area with y=1.
     *
     * @param location The current location of the character
     * @return An ASCII-based map of the dungeon
     */
    protected String generateDungeonMap(AreaState location) {

        /*
         * Scales better than some other methods for drawing the map b/c we're only looping the
         * collection of areas once.
         */

        final DungeonScale scale = calculateDungeonScale(location.getAllAreasInMaze());
        final Map<Integer,List<String>> rawRows = generateRows(scale);
        final Map<Integer,List<String>> filledRows = addAreas(rawRows, scale, location);

        final String rslt = flatten(filledRows, scale);
        logger.log(Level.FINE, "Generated the following dungeon map for location: {0}\n{1}",
                new Object[] { location, rslt });
        return rslt;

    }

    private DungeonScale calculateDungeonScale(Set<AreaState> areas) {

        // We need to know the scale of the map before we begin
        int minX = Integer.MAX_VALUE;
        int minY = Integer.MAX_VALUE;
        int maxX = Integer.MIN_VALUE;
        int maxY = Integer.MIN_VALUE;

        for (AreaState area : areas) {
            minX = minX < area.getX()
                    ? minX
                    : area.getX();
            minY = minY < area.getY()
                    ? minY
                    : area.getY();
            maxX = maxX > area.getX()
                    ? maxX
                    : area.getX();
            maxY = maxY > area.getY()
                    ? maxY
                    : area.getY();
        }
        return new DungeonScale(minX, minY, maxX, maxY);

    }

    /**
     * Produces a multi-dimensional data structure based on the range of Y values in
     * <code>scale</code>.  The keys are the possible Y positions.  The values are 3 lines filled
     * with the '@' character.
     */
    private Map<Integer,List<String>> generateRows(DungeonScale scale) {

        final int numColumns = scale.getMaxX() - (scale.getMinX() - 1) + /* outer walls */ 2;
        final String lineTemplate =
                new String(new char[numColumns * 3]).replace("\0", BLOCKED_CHARACTER) + "\n";
        final List<String> rowTemplate =
                Arrays.asList(lineTemplate, lineTemplate, lineTemplate);

        final Map<Integer,List<String>> rslt = new HashMap<>();
        for(int yPos = scale.getMaxY() + /* top wall */ 1; yPos >= scale.getMinY(); yPos--) {
            rslt.put(yPos, new ArrayList<>(rowTemplate));
        }
        return rslt;

    }

    private Map<Integer,List<String>> addAreas(Map<Integer, List<String>> rows, DungeonScale scale,
            AreaState location) {

        location.getAllAreasInMaze()
                .forEach(area -> {
                    final List<String> beforeRow = rows.get(area.getY());
                    final int relativeX = area.getX() - scale.getMinX() + /* outer wall */ 1;
                    final List<String> afterRow = new ArrayList<>();
                    for (int i=0; i < 3; i++) {
                        final String lineTemplate = beforeRow.get(i);
                        String afterLine;
                        if (i == 1 && location.equals(area)) {
                            afterLine = lineTemplate.substring(0, relativeX * 3)
                                    + LOCATION_MARKER_CHUNK
                                    + lineTemplate.substring((relativeX + 1) * 3, lineTemplate.length());
                        } else {
                            afterLine = lineTemplate.substring(0, relativeX * 3)
                                    + UNBLOCKED_CHUNK
                                    + lineTemplate.substring((relativeX + 1) * 3, lineTemplate.length());
                        }
                        afterRow.add(afterLine);
                    }
                    rows.put(area.getY(), afterRow);
                });
        return rows;

    }

    private String flatten(Map<Integer, List<String>> rows, DungeonScale scale) {
        return IntStream.iterate(scale.getMaxY() + 1, i -> --i)
                .limit(scale.getMaxY() - (scale.getMinY() - 1) + /* top wall */1)
                .mapToObj(yPos -> {
                    final List<String> row = rows.get(yPos);
                    return row.stream().collect(Collectors.joining());
                }).collect(Collectors.joining());
    }

    private static final class DungeonScale {
        private final int minX;
        private final int minY;
        private final int maxX;
        private final int maxY;

        public DungeonScale(int minX, int minY, int maxX, int maxY) {
            this.minX = minX;
            this.minY = minY;
            this.maxX = maxX;
            this.maxY = maxY;
        }

        public int getMinX() {
            return minX;
        }

        public int getMinY() {
            return minY;
        }

        public int getMaxX() {
            return maxX;
        }

        public int getMaxY() {
            return maxY;
        }

        @Override
        public String toString() {
            return "DungeonScale{" +
                    "minX=" + minX +
                    ", minY=" + minY +
                    ", maxX=" + maxX +
                    ", maxY=" + maxY +
                    '}';
        }

    }

}
