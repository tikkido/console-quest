package org.drewwills.challenge.cquest.ui;

import org.drewwills.challenge.cquest.AbstractManagedObject;
import org.drewwills.challenge.cquest.Assert;
import org.drewwills.challenge.cquest.Check;
import org.drewwills.challenge.cquest.Option;
import org.drewwills.challenge.cquest.UserInterface;

import javax.annotation.PostConstruct;
import java.io.Console;
import java.util.Collection;
import java.util.Map;

/**
 * Concrete implementation of {@link UserInterface} based on <code>java.io.Console</code>.
 */
public class ConsoleUserInterface extends AbstractManagedObject implements UserInterface {

    // Basic colors
    protected static final String BASIC_BLACK = "\u001b[30m";
    protected static final String BASIC_RED = "\u001b[31m";
    protected static final String BASIC_GREEN = "\u001b[32m";
    protected static final String BASIC_YELLOW = "\u001b[33m";
    protected static final String BASIC_BLUE = "\u001b[34m";
    protected static final String BASIC_MAGENTA = "\u001b[35m";
    protected static final String BASIC_CYAN = "\u001b[36m";
    protected static final String BASIC_WHITE = "\u001b[37m";

    // ANSI BRIGHT COLORS
    protected static final String BRIGHT_BLACK = "\u001b[30;1m";
    protected static final String BRIGHT_RED = "\u001b[31;1m";
    protected static final String BRIGHT_GREEN = "\u001b[32;1m";
    protected static final String BRIGHT_YELLOW = "\u001b[33;1m";
    protected static final String BRIGHT_BLUE = "\u001b[34;1m";
    protected static final String BRIGHT_MAGENTA = "\u001b[35;1m";
    protected static final String BRIGHT_CYAN = "\u001b[36;1m";
    protected static final String BRIGHT_WHITE = "\u001b[37;1m";

    // Reset
    protected static final String RESET_CODE = "\u001b[0m";

    private static final long COMBAT_LOG_PAUSE_DURATION_MILLIS = 750;

    private Console console;

    @PostConstruct
    public void setup() {
        // validate the availability of the Console...
        console = System.console();
        if (console == null) {
            throw new RuntimeException("The JVM has been launched in non-interactive mode; " +
                    "the Console is not available.");
        }
    }

    @Override
    public String formatText(String text, TextFormat format) {
        String colorCode;
        switch(format) {
            case HIGHLIGHT:
                colorCode = BRIGHT_BLUE;
                break;
            case PROMPT:
                colorCode = BRIGHT_GREEN;
                break;
            case WARNING:
                colorCode = BRIGHT_RED;
                break;
            case ERROR:
                colorCode = BRIGHT_RED;
                break;
            default:
                throw new UnsupportedOperationException(
                        "Unrecognized TextFormat option: " + format);
        }
        return colorCode + text + RESET_CODE;
    }

    @Override
    public void displayMessage(String format, boolean prefixBlank, Object... args) {
        Assert.notBlank(format, "Argument 'format' cannot be blank");

        if (prefixBlank) {
            /*
             * In most cases we're going to display a blank line before the message to make it
             * easier to read.
             */
            console.printf("\n");
        }
        console.printf(format, args);
        console.printf("\n"); // Also terminate the line after the message
    }

    /**
     * In the console-based {@link UserInterface} we would like to pause (briefly) after each
     * message to the combat log for dramatic effect and to encourage the user to read it.  Without
     * this pause, several messages would appear at once.
     *
     * @param format A format string as described by <code>java.util.Formatter</code>
     * @param args Arguments referenced by the format specifiers in the format string (if any)
     */
    @Override
    public void combatLog(String format, Object... args) {
        UserInterface.super.combatLog(format, args);
        try {
            Thread.sleep(COMBAT_LOG_PAUSE_DURATION_MILLIS);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T extends Option> T promptForSelection(Collection<T> options,
            SelectionStrategy selectionStrategy) {
        Assert.notEmpty(options, "Argument 'options' cannot be null or empty");
        Assert.notNull(selectionStrategy, "Argument 'selectionStrategy' cannot be null");

        // Prepare the prompt
        final StringBuilder optionsText = new StringBuilder();
        optionsText.append("Choose one of the following options:\n");
        final Map<String,T> optionsMap = selectionStrategy.prepareOptions(options);
        optionsMap.entrySet().stream()
                .forEach(entry -> optionsText.append(
                        String.format("  (%s) -> %s\n", entry.getKey(),
                                entry.getValue().getText())));
        optionsText.append(formatText("Your selection", TextFormat.PROMPT));

        // Optain a selection from the user
        T rslt = null;
        while (rslt == null) {

            // Capture user input
            String userInput = promptForInput(optionsText.toString());

            // Did the user enter something?
            if (Check.isBlank(userInput)) {
                displayError("Please select an option from the list.");
                continue;
            }

            // Does the number match an available option?
            if (!optionsMap.containsKey(userInput)) {
                displayError("Invalid input; please enter one of the following: ", optionsMap.keySet());
                continue;
            }

            // Choose the selected option and proceed
            rslt = optionsMap.get(userInput);

        }

        return rslt;
    }

    @Override
    public String promptForInput(String format, Object... args) {
        Assert.notBlank(format, "Argument 'format' cannot be blank");

        // Precede every prompt with a blank line...
        console.printf("\n");

        // Add a colon and a space following every prompt...
        return console.readLine(format + ": ", args);
    }

}
