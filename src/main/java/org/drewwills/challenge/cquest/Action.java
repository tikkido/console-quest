package org.drewwills.challenge.cquest;

/**
 * Something that can be performed by a {@link Combatant} in combat.
 */
public interface Action extends ManagedObject {

    String getName();

    /**
     * This action is performed by the <code>source</code> upon the <code>opponent</code> (if
     * applicable).  A non-null response moves the character to the specified {@link State}.
     *
     * @param source The {@link Combatant} performing the action
     * @param opponent The {@link Combatant} upon whom the action is performed (if applicable)
     * @return Normally <code>null</code>, but a non-null value moves the character directly there
     */
    State perform(Combatant source, Combatant opponent);

}
