package org.drewwills.challenge.cquest;

import java.util.Set;

/**
 * Represents a position (on a map) that characters can move in and out of.
 */
public interface AreaState extends InteractiveState, Comparable<AreaState> {

    Mob getMob();

    boolean hasNorth();

    AreaState getNorth();

    boolean hasEast();

    AreaState getEast();

    boolean hasSouth();

    AreaState getSouth();

    boolean hasWest();

    AreaState getWest();

    boolean isExit();

    int getX();

    int getY();

    /**
     * Obtains a collection containing all the {@link AreaState} objects in the same {@link Maze} as
     * this one.
     */
    Set<AreaState> getAllAreasInMaze();

}
