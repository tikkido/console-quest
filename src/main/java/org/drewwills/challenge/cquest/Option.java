package org.drewwills.challenge.cquest;

/**
 * Represents a choice that the user can make.  Every {@link State} offers several options;
 * choosing one leads to the next state.
 */
public interface Option extends ManagedObject {

    /**
     * UI text explaining this option to the user.
     */
    String getText();

}
