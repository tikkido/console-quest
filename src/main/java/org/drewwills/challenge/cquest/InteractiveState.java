package org.drewwills.challenge.cquest;

/**
 * States that require special interaction with the user should implement this interface.  Examples
 * include entering text or choosing from an enumerated set of options (but not {@link Option}
 * objects).  When present, the {@link StateMachine} will hand control over to the state after
 * rendering but before presenting the user with options for proceeding in the game.
 */
public interface InteractiveState extends State {

    /**
     * Execute special interaction with the user.
     *
     * @return <code>null</code> to continue normally, or a valid {@link State} object to change to
     * a different state.
     */
    State interact();

}
