package org.drewwills.challenge.cquest;

/**
 * Encapsulates the network of areas (map) of a dungeon.  Object of this type are produced as-needed
 * for replayability.  This interface is a simple data structure;  it does <em>not</em> extend
 * {@link ManagedObject}.
 */
public interface Maze {

    /**
     * Obtains the {@link AreaState} through which a {@link PlayerCharacter} enters the maze.  The
     * position of this area on the map is always x=1, y=1.
     */
    AreaState getEntrance();

}
