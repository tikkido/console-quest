package org.drewwills.challenge.cquest;

import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * This Singleton class has access to the {@link Environment} and knows how to create and initialize
 * instances of {@link ManagedObject}.
 */
public class ManagedObjectService {

    private final Environment environment;

    /**
     * Creates a new managed object of unknown concrete type but known subtype (usually an
     * interface).
     *
     * @param className The fully-qualified name of the concrete class
     * @param <T> The desired return type
     * @return An object with concrete type <code>className</code> but reference type <code>T</code>
     */
    public <T extends ManagedObject> T create(String className, Class<T> clazz) {
        Assert.notBlank(className, "Argument 'className' cannot be null");

        try {
            Class<T> concrete = (Class<T>) Class.forName(className);
            return create(concrete);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Failed to initialize an object of the specified type: " + className, e);
        }
    }

    /**
     * Creates a new managed object based on a known concrete class.
     *
     * @param <T> The desired return type
     * @return An object with concrete type <code>T</code>
     */
    public <T extends ManagedObject> T create(Class<T> clazz) {
        Assert.notNull(clazz, "Argument 'clazz' cannot be null");

        try {
            final T rslt = clazz.newInstance();
            return manage(rslt);
        } catch (Exception e) {
            throw new RuntimeException("Failed to create an object of the specified type: " +
                    clazz.getName(), e);
        }
    }

    /**
     * Initiates management of a {@link ManagedObject} created outside of this service.
     *
     * @param object The object to manage
     * @param <T> The desired return type
     * @return The original object passed to this method, now under management
     */
    public <T extends ManagedObject> T manage(T object) {
        Assert.notNull(object, "Argument 'object' cannot be null");

        try {
            object.init(environment);
            /*
             * Process methods annotated with @PostConstruct (do we need to process other levels of
             * the type hierarchy?)
             */
            for (Method m : object.getClass().getMethods()) {
                if (m.isAnnotationPresent(PostConstruct.class)) {
                    try {
                        m.invoke(object);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        throw new RuntimeException(
                                "Failed to invoke @PostConstruct method defined by class: " +
                                        getClass(), e);
                    }
                }
            }
            return object;
        } catch (Exception e) {
            throw new RuntimeException("Failed to initialize an object of the specified type: " +
                    object.getClass().getName(), e);
        }
    }

    /* package-private */ ManagedObjectService(Environment environment) {
        this.environment = environment;
    }

}
