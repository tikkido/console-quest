package org.drewwills.challenge.cquest;

/**
 * The Adventurers' Guild Hall, where players can create a character or load a saved game.
 */
public class GuildHallState extends AbstractState implements InteractiveState {

    private static final String TEXT =
            "The Adventurers' Guild Hall is renowned far and wide for the unsavory crowd who " +
            "frequent it.  A wise patron one said \"you will never find a more wretched hive of " +
            "scum and villainy.\"";

    public GuildHallState() {
        super(TEXT);
    }

    @Override
    public State interact() {
        final SavedGame savedGame = getEnv().getSavedGame();
        if (savedGame != null) {
            final UserInterface ui = getEnv().getUserInterface();
            ui.displayMessage("You are logged in as %s",
                    ui.formatText(savedGame.getPlayerCharacter().getName(),
                            UserInterface.TextFormat.HIGHLIGHT));
        }
        return null; // Carry on...
    }

}
