package org.drewwills.challenge.cquest.combat;

import org.drewwills.challenge.cquest.AbstractManagedObject;
import org.drewwills.challenge.cquest.Action;
import org.drewwills.challenge.cquest.Assert;
import org.drewwills.challenge.cquest.CombatEngine;
import org.drewwills.challenge.cquest.Combatant;
import org.drewwills.challenge.cquest.Mob;
import org.drewwills.challenge.cquest.PayloadOption;
import org.drewwills.challenge.cquest.PlayerCharacter;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * A fairly bare-bones strategy for how combat works.
 */
public class BasicCombatEngine extends AbstractManagedObject implements CombatEngine {

    /**
     * Name of the application property that defines the complete collection of options available to
     * the player in combat.
     */
    private static final String AVAILABLE_ACTIONS_PROPERTY =
            BasicCombatEngine.class.getName() + ".combatActions";

    /**
     * For measuring the impact (on combat) of the difference between two attribute values:  one
     * from the attacker, one from the defender.  Creates a curve where y is around:
     *
     * <ul>
     *   <li>0 when x is 1</li>
     *   <li>2 when x is 2</li>
     *   <li>6 when x is 5</li>
     *   <li>9 when x is 10</li>
     *   <li>13 when x is 30</li>
     * </ul>
     *
     * <p>So it rises quickly (starting with 2), but offers diminishing returns as the difference
     * gets greater.
     */
    private static final double TO_HIT_LOG_BASE = 1.3D;

    private static final double TO_HIT_THRESHOLD = 50.0D;
    private static final double CRITICAL_HIT_THRESHOLD = 87.0D;
    private static final double ESCAPE_THRESHOLD = 50.0D;
    private static final int DAMAGE_CONSTANT = 2;
    private static final int CRITICAL_HIT_MULTIPLIER = 2;

    private List<PayloadOption<Action>> combatOptions;
    private Action mobAttackAction;

    private final Logger logger = Logger.getLogger(getClass().getName());

    @PostConstruct
    public void setup() {

        // Combat options
        final String availableActionsProperty = getEnv().getProperties().get(AVAILABLE_ACTIONS_PROPERTY);
        Assert.notBlank(availableActionsProperty,
                "Required property is blank: " + AVAILABLE_ACTIONS_PROPERTY);
        final List<Action> actions = Arrays.stream(availableActionsProperty.split(","))
                .map(actionClass -> getEnv().getObjectService().create(actionClass, Action.class))
                .collect(Collectors.toList());
        logger.log(Level.INFO,"Found the following available actions: {0}", actions);
        final List<PayloadOption<Action>> optionsList = actions.stream()
                .map(action -> new PayloadOption<>(action.getName(), action))
                .map(option -> getEnv().getObjectService().manage(option))
                .collect(Collectors.toList());
        combatOptions = Collections.unmodifiableList(optionsList);

        // Mobs are not very intelligent
        mobAttackAction = getEnv().getObjectService().create(AttackAction.class);

    }

    @Override
    public List<PayloadOption<Action>> getCombatOptions() {
        return combatOptions;
    }

    /**
     * This {@link CombatEngine} strategy is <em>basic</em>.  Living mobs always attack.
     */
    @Override
    public Action chooseMobAction(Mob mob) {
        return mobAttackAction;
    }

    @Override
    public ToHitOutcome attemptToHit(Combatant attacker, Combatant defender) {

        // Chance to hit increases or decreases based on difference in weapon skill
        final double skillFactor = calculateFactor(attacker.getWeaponSkill(),
                defender.getWeaponSkill());
        final double score = Dice.d100() + skillFactor;

        ToHitOutcome rslt = ToHitOutcome.MISS; // default
        if (score > CRITICAL_HIT_THRESHOLD) {
            rslt = ToHitOutcome.CRITICAL_HIT;
        } else if (score > TO_HIT_THRESHOLD) {
            rslt = ToHitOutcome.HIT;
        }
        logger.log(Level.INFO, "Calculated to hit score of {0} using skillFactor {1}: {2}!",
                new Object[] { score, skillFactor, rslt });

        return rslt;

    }

    @Override
    public boolean attemptToFlee(Combatant hunted, Combatant hunter) {

        // Chance to escape increases or decreases based on difference in speed
        final double skillFactor = calculateFactor(hunted.getSpeed(),
                hunter.getSpeed());
        final double score = Dice.d100() + skillFactor;

        boolean rslt = score > ESCAPE_THRESHOLD;

        logger.log(Level.INFO, "Calculated to flee score of {0} using skillFactor {1}, " +
                "outcome: {2}", new Object[] { score, skillFactor, rslt });

        return rslt;

    }

    @Override
    public int calculateDamage(Combatant attacker, Combatant defender, ToHitOutcome toHitOutcome) {

        /*
         * Amount of damage increases or decreases based on the difference between the attacker's
         * attackPower and the defender's armor
         */
        final double damageFactor = calculateFactor(attacker.getAttackPower(), defender.getArmor());
        final double score = Dice.d6() + damageFactor;
        final double factorAdjustment = score > 0
                ? score
                : 0.9D; // minimum
        final int rawDamage = (int) (DAMAGE_CONSTANT + Math.ceil(factorAdjustment));

        int rslt = rawDamage; // default
        if (ToHitOutcome.CRITICAL_HIT.equals(toHitOutcome)) {
            rslt = rawDamage * CRITICAL_HIT_MULTIPLIER;
        }
        logger.log(Level.INFO, "Calculated damage of {0} based on rawDamage of {1} and " +
                "ToHitOutcome {2}", new Object[] { rslt, rawDamage, toHitOutcome });

        return rslt;

    }

    /**
     * Add the mob's attributes to arrive at a value.  A different {@link CombatEngine} that is less
     * <em>basic</em> might factor in the relative strength of the combatants.
     */
    @Override
    public long calculateExperience(PlayerCharacter playerCharacter, Mob mob) {
        return mob.getWeaponSkill() + mob.getAttackPower() + mob.getArmor() + mob.getMaxHealth();
    }

    /**
     * A "factor" is the impact of the difference between two values:  one from the attacker, one
     * from the defender.  This factor increases or decreases logarithmically based on that
     * difference.
     *
     * <p>This implementation produces a non-zero factor when the difference is <code>2.0D</code> or
     * more.
     */
    private double calculateFactor(double attackerValue, double defenderValue) {
        double rslt = 0.0D; // default
        if (attackerValue != defenderValue) {
            final double difference = Math.abs(attackerValue - defenderValue);
            rslt = Math.log(difference) / Math.log(TO_HIT_LOG_BASE);
            if (attackerValue < defenderValue) {
                rslt = -rslt;
            }
        }
        logger.log(Level.INFO, "Calculated an impact factor of {0} based on attackerValue {1} " +
                        "and defenderValue {2}", new Object[] { rslt, attackerValue, defenderValue });
        return rslt;
    }

}
