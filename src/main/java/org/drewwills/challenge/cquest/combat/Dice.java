package org.drewwills.challenge.cquest.combat;

import java.util.Random;

/**
 * Loosely represents rolling different types of dice.  The return types are <code>double</code>,
 * rather than <code>int</code>, since most combat math is floating point.
 */
public class Dice {

    private static final Random RANDOM = new Random();

    public static double d4() {
        return RANDOM.nextDouble() * 4.0D;
    }

    public static double d6() {
        return RANDOM.nextDouble() * 6.0D;
    }

    public static double d10() {
        return RANDOM.nextDouble() * 10.0D;
    }

    public static double d12() {
        return RANDOM.nextDouble() * 12.0D;
    }

    public static double d20() {
        return RANDOM.nextDouble() * 20.0D;
    }

    public static double d100() {
        return RANDOM.nextDouble() * 100.0D;
    }

    /**
     * Private constructor; this class is not instantiated.
     */
    private Dice() {}

}
