package org.drewwills.challenge.cquest.combat;

import org.drewwills.challenge.cquest.AbstractAction;
import org.drewwills.challenge.cquest.CombatEngine;
import org.drewwills.challenge.cquest.Combatant;
import org.drewwills.challenge.cquest.State;
import org.drewwills.challenge.cquest.UserInterface;

public class FleeAction extends AbstractAction {

    public FleeAction() {
        super("Flee!");
    }

    @Override
    public State perform(Combatant source, Combatant opponent) {

        final UserInterface ui = getEnv().getUserInterface();
        ui.combatLog("%s attempts to flee...", source.getName());

        final CombatEngine combatEngine = getEnv().getCombatEngine();
        final boolean escape = combatEngine.attemptToFlee(source, opponent);
        if (escape) {
            ui.combatLog(ui.formatText("SUCCESS!", UserInterface.TextFormat.HIGHLIGHT));
            // Send the character back where s/he came from
            return getEnv().getSavedGame().getLocation();
        } else {
            ui.combatLog(ui.formatText("FAILURE!", UserInterface.TextFormat.WARNING));
            return null;
        }

    }

}
