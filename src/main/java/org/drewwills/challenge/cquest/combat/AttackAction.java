package org.drewwills.challenge.cquest.combat;

import org.drewwills.challenge.cquest.AbstractAction;
import org.drewwills.challenge.cquest.CombatEngine;
import org.drewwills.challenge.cquest.Combatant;
import org.drewwills.challenge.cquest.State;
import org.drewwills.challenge.cquest.UserInterface;

public class AttackAction extends AbstractAction {

    public AttackAction() {
        super("Attack!");
    }

    @Override
    public State perform(Combatant source, Combatant opponent) {

        final UserInterface ui = getEnv().getUserInterface();
        ui.combatLog("%s attacks %s", source.getName(), opponent.getName());

        final CombatEngine combatEngine = getEnv().getCombatEngine();
        final CombatEngine.ToHitOutcome hitOutcome = combatEngine.attemptToHit(source, opponent);
        switch (hitOutcome) {
            case MISS:
                ui.combatLog("MISS!");
                break;
            case HIT:
                ui.combatLog(ui.formatText("HIT!", UserInterface.TextFormat.HIGHLIGHT));
                break;
            case CRITICAL_HIT:
                ui.combatLog(ui.formatText("CRITICAL HIT!", UserInterface.TextFormat.HIGHLIGHT));
                break;
            default:
                throw new IllegalArgumentException("Unrecognized ToHitOutcome: " + hitOutcome);
        }

        if (hitOutcome.isHit()) {
            final int damage = combatEngine.calculateDamage(source, opponent, hitOutcome);
            ui.combatLog("%s does %d damage to %s", source.getName(), damage, opponent.getName());
            opponent.inflict(damage);
        }

        return null;

    }

}
