package org.drewwills.challenge.cquest;

/**
 * Base class for {@link State} classes that provides an implementation of <code>getText</code>.
 */
public abstract class AbstractState extends AbstractManagedObject implements State {

    private static final long serialVersionUID = 1L;

    private final String text;

    @Override
    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return String.format("%s{text='%s'}", getClass().getSimpleName(), getText());
    }

    protected AbstractState(String text) {
        this.text = text;
    }

}
