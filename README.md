![Console Quest Logo](etc/console-quest-logo.png)

## Welcome to _Console Quest!_

Console Quest is a text-based Role Playing Game (RPG) developed in Java.  Create your character and
explore dangerous dungeons, encountering fantastical beasts along the way!

## Prerequisites

The following software packages are required for working with Console Quest:

  - A [Java Development Kit][] (JDK)
  - A suitable [Git Client][] for your OS

**NOTE:** Instead of using Git, if you prefer, you can also [download the project as a .zip file].

## Cloning and Building the Project

Use the following commands to obtain the project source code from BitBucket.org and build it
locally:

```console
$ git clone https://drewwills@bitbucket.org/tikkido/console-quest.git
$ cd console-quest
$ ./gradlew build
```

## Running the Project

Once you have a local copy of the packaged software, you can run Console Quest with the following
command:

```console
$ java -jar build/libs/console-quest-0.8-SNAPSHOT.jar
```

## Configuration

Two properties files govern all configuration in Console Quest:  `logger.properties` and
`application.properties`.  Both of these files can be found in the `src/main/resources` directory
within the project source files. The first of these (`logger.properties`) controls application
logging, and it follows the [standard log file format] for `java.util.logging.LogManager`.

The second file (`application.properties`) contains configuration settings that are custom to
Console Quest.  Refer to the table below for information about them.

### Console Quest Custom Configuration

| Property | Description |
| --- | --- |
| `org.drewwills.challenge.cquest.Environment.userInterface` | Fully-qualified name of the concrete UserInterface class |
| `org.drewwills.challenge.cquest.Environment.stateMachineOptions` | Comma-delimited list of options presented by the `StateMachine` |
| `org.drewwills.challenge.cquest.Environment.stateMachine` | Fully-qualified name of the concrete StateMachine class |
| `org.drewwills.challenge.cquest.Environment.savedGameService` | Fully-qualified name of the concrete SavedGameService class |
| `org.drewwills.challenge.cquest.Environment.combatEngine` | Fully-qualified name of the concrete CombatEngine class |
| `org.drewwills.challenge.cquest.guild.SelectArchetypeState.availableArchetypes` | Comma-delimited list of archetypes available to the player when creating a new character |
| `org.drewwills.challenge.cquest.guild.SelectDungeonState.availableDungeons` | Comma-delimited list of dungeons available to explore |
| `org.drewwills.challenge.cquest.io.SerializableSavedGameService.savedGameDirectory` | File system location of saved game files |
| `org.drewwills.challenge.cquest.combat.BasicCombatEngine.combatActions` | Comma-delimited list of the concrete Action classes that will be offered to the user during each combat round |

## Extending Console Quest

Nearly all of the configuration properties listed above take fully-qualified Java class names as
their value(s).  One way to extend Console Quest would be to change one or more of these properties
to point to new Java classes that you create.

For example, the `org.drewwills.challenge.cquest.Environment.userInterface` property defines the
User Interface (UI) for Console Quest.  The current UI class interacts with the user through the
console (terminal), but you could replace this UI with one that interacts _via_ a browser window.
In this example, your UI class must implement `org.drewwills.challenge.cquest.UserInterface`.

Another, easier way to extend Console Quest is to add more content...

### Adding a New Archetype

Adding a new character archetype is fairly simple.  You need 2 new Java classes:  one that
implements `Archytype` (typically by extending `AbstractArchetype`), and one that implements
`PlayerCharacter` (typically by extending `AbstractPlayerCharacter`).  **NOTE:** if you don't extend
the provided abstract classes, you'll have more work to do (and a bit more to learn about the code).

#### Example Archetype

```java
package org.drewwills.challenge.cquest.guild.archetype;

import org.drewwills.challenge.cquest.PlayerCharacter;
import org.drewwills.challenge.cquest.guild.Archetype;

/**
 * {@link Archetype} implementation for {@link Berserker} characters.
 */
public class BerserkerArchetype extends AbstractArchetype {

    public BerserkerArchetype() {
        super("Berserker",
                "A warrior with high damage and health, low weapon skill and armor");
    }

    @Override
    public PlayerCharacter create(String name) {
        return new Berserker(name);
    }

}
```

#### Example PlayerCharacter

```java
package org.drewwills.challenge.cquest.guild.archetype;

import org.drewwills.challenge.cquest.PlayerCharacter;
import org.drewwills.challenge.cquest.guild.AbstractPlayerCharacter;

/**
 * Warrior with high damage and health, low weapon skill and armor.
 */
public class Berserker extends AbstractPlayerCharacter {

    /* package-private */ Berserker(String name) {
        super(name, 14, 26, 12, 28);
    }

}
```

Lastly, you will need to register your archetype with the
`org.drewwills.challenge.cquest.guild.SelectArchetypeState.availableArchetypes` property in
`application.properties`.

### Adding a New Dungeon

Adding a new `Dungeon` is also easy.  Use the `MazeBuilder` class.

#### Example Dungeon

```java
package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.Dungeon;
import org.drewwills.challenge.cquest.ManagedObjectService;
import org.drewwills.challenge.cquest.Maze;

/**
 * A very simple {@link Dungeon}.
 */
public class VerySimpleDungeon extends AbstractDungeon {

    public VerySimpleDungeon() {
        super("Very Simple Dungeon");
    }

    @Override
    public Maze getMaze() {
        final ManagedObjectService objectService = getEnv().getObjectService();
        final MazeBuilder mazeBuilder =
                MazeBuilder.enter("You're in a scary dungeon", getEnv())
                        .goNorth("Now you're in a scary hallway")
                        .encounter(objectService.create(GoblinSpearman.class))
                        .exit();
        return mazeBuilder.build();
    }

}
```

Lastly, you will need to register your dungeon with the
`org.drewwills.challenge.cquest.guild.SelectDungeonState.availableDungeons` property in
`application.properties`.

### Adding a New Mob

Adding a new `Mob` is easiest of all:  just create one Java class.

#### Example Mob

```java
package org.drewwills.challenge.cquest.dungeon;

import org.drewwills.challenge.cquest.AbstractCombatant;
import org.drewwills.challenge.cquest.Mob;

/**
 * About equal to the character in combat.
 */
public class SkeletalKnight extends AbstractCombatant implements Mob {

    public SkeletalKnight() {
        super("Skeletal Knight", 18, 18, 24, 20, 18);
    }

}
```

You **don't need to add** your `Mob` class to `application.properties`, but (if you want to meet it
in the game) you will need to use it (in a dungeon) with `MazeBuilder`.

## That's All!

Good luck, and have fun!

[Java Development Kit]: http://www.oracle.com/technetwork/java/javase/downloads/index.html
[Git Client]: https://git-scm.com/downloads
[download the project as a .zip file]: https://bitbucket.org/tikkido/console-quest/get/ce2886349497.zip
[standard log file format]: https://docs.oracle.com/javase/8/docs/api/java/util/logging/LogManager.html
